﻿using Motive.Unity.AR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostHunterUIManager : UIModeManager<GhostHunterUIManager> 
{
    public Mode CurrentMode { get; private set; }

    public enum Mode
    {
        Map,
        AR
    }

    protected override void Start()
    {
        DeactivateMode(Mode.AR);
        ActivateMode(Mode.Map);

        base.Start();
    }



    public void SetMode(Mode mode)
    {
        if (CurrentMode != mode)
        {
            DeactivateMode(CurrentMode);
            ActivateMode(mode);
        }
    }

    public void ActivateMode(Mode mode)
    {
        CurrentMode = mode;

        switch (mode)
        {
            case Mode.AR:
				SetARActive(true);
                break;
            case Mode.Map:
				SetMapActive(true);
                break;
        }
    }

    public void DeactivateMode(Mode mode)
    {
        switch (mode)
		{
			case Mode.AR:
				SetARActive(false);
				break;
			case Mode.Map:
				SetMapActive(false);
				break;
        }
    }

    public override void SetARMode()
    {
        SetMode(Mode.AR);
    }

    public override void SetMapMode()
    {
        SetMode(Mode.Map);
    }
}

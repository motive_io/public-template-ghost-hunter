﻿using Motive.AR.Models;
using Motive.Gaming.Models;
using Motive.UI.Framework;
using Motive.Unity;
using Motive.Unity.AR;
using Motive.Unity.Maps;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Cameras;

/// <summary>
/// Manages the ghost trapping "minigame".
/// </summary>
public class GhostHunterTrapPanel : Panel<MapAnnotation<LocationTaskDriver>>
{
    public InventoryTableItem TrapItemPrefab;
    public Table TrapTable;
    public Text AlertText;
    public GameObject AlertPane;
    public Image TargetBox;
    public GameObject DefaultGhostWorldObject;

    private ARWorldObject m_worldObj;
    private Collectible m_ghost;

    public override void DidPush()
    {
        ARQuestAppUIManager.Instance.SetARMode();

        base.DidPush();
    }

    public override void DidPop()
    {
        if (m_worldObj != null)
        {
            ARWorld.Instance.RemoveWorldObject(m_worldObj);
        }

        ARQuestAppUIManager.Instance.SetARMode();

        base.DidPop();
    }

    void Alert(string message)
    {
        AlertPane.SetActive(true);
        AlertText.text = message;
    }

    void SpawnGhost(GameObject ghostPrefab)
    {
        if (!ghostPrefab)
        {
            Alert("Could not spawn ghost!");

            return;
        }

        m_worldObj = new ARWorldObject();

        // Limit the range of the ghost in the AR view so that we can 
        // always see it.        
        m_worldObj.Options = new LocationAugmentedOptions
        {
            DistanceVariation = new LinearDistanceVariation
            {
                Range = new Motive.Core.Models.DoubleRange { Min = 3, Max = 5 }
            }
        };

        m_worldObj.GameObject = Instantiate(ghostPrefab);
        m_worldObj.Location = Data.Data.Task.Locations.FirstOrDefault();
		var lookAt = m_worldObj.GameObject.GetComponent<LookatTarget>();

		if (lookAt != null)
		{
			lookAt.SetTarget(ARWorld.Instance.ARAdapter.WorldCamera.transform);
		}

        ARWorld.Instance.AddWorldObject(m_worldObj);

    }

    public override void Populate(MapAnnotation<LocationTaskDriver> annotation)
    {
        ARQuestAppUIManager.Instance.SetARMode();

        AlertPane.SetActive(false);

        TrapTable.Clear();


        m_ghost = annotation.Data.FirstCollectible;

        if (m_ghost == null)
        {
            Alert("No ghosts!");
            return;
        }

        // If the ghost collectible includes a model, spawn that instead
        // of the default.
        if (m_ghost.AssetInstance != null && m_ghost.AssetInstance.Asset is UnityAsset)
        {
            StartCoroutine(UnityAssetLoader.LoadAsset<GameObject>((UnityAsset)m_ghost.AssetInstance.Asset, SpawnGhost));
        }
        else
        {
            SpawnGhost(DefaultGhostWorldObject);
        }

        if (m_ghost != null)
        {
            PopulateTraps();
        }
    }

    void PopulateTraps()
    {
        TrapTable.Clear();

        var traps = Inventory.Instance.GetItemsForActionOnEntity(m_ghost, "trap");

        if (traps == null || traps.Count() == 0)
        {
            Alert("You don't have a trap that can catch this ghost!");
            return;
        }
        else
        {
            foreach (var t in traps)
            {
                var item = TrapTable.AddItem(TrapItemPrefab);
                item.Populate(t.Collectible);
                item.OnSelected.AddListener(() => item_Selected(item));
            }
        }
    }

    void item_Selected(InventoryTableItem item)
    {
        var count = Inventory.Instance.GetCount(item.Collectible.Id);

        if (count == 0)
        {
            var recipes = RecipeDirectory.Instance.GetRecipesForCollectible(item.Collectible.Id);

            // For now only look at one recipe per collectible
            var recipe = (recipes == null) ? null : recipes.FirstOrDefault();

            PanelManager.Instance.Push<ExecuteRecipePanel>(recipe, () =>
                {
                    PopulateTraps();
                });
        }
        else
        {
            Inventory.Instance.Remove(item.Collectible.Id, 1);

            TransactionManager.Instance.AddValuables(Data.Data.Task.ActionItems);

            // Push the reward panel then close this panel + AR mode.
            PanelManager.Instance.Push<GhostRewardPanel>(m_ghost, () =>
                {
                    // TODO: this is a hack, this should go through the driver.
                    MapController.Instance.RemoveAnnotation(Data);

                    Close();
                });
        }
    }

    void SetInRange(bool inRange)
    {
        TargetBox.color = inRange ? Color.green : Color.red;

        foreach (var obj in TrapTable.Items)
        {
            var item = obj.GetComponent<InventoryTableItem>();

            var itemCount = Inventory.Instance.GetCount(item.Collectible.Id);

            // If we don't have any items, then clicking brings up the 
            // execute recipe panel.
            item.Selectable = itemCount == 0 || inRange;
        }
    }

    void Update()
    {
        bool inRange = false;

        if (m_worldObj != null && TargetBox)
        {
            var screenPoint = ARWorld.Instance.MainCamera.WorldToScreenPoint(m_worldObj.GameObject.transform.position);

            inRange = RectTransformUtility.RectangleContainsScreenPoint(TargetBox.rectTransform, screenPoint);
        }

        SetInRange(inRange);
    }
}

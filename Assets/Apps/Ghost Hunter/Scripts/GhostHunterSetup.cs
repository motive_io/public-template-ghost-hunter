using UnityEngine;
using System.Collections;
using Motive.AR.Scripting;
using Motive.AR.LocationServices;
using Motive.UI.Framework;
using Motive;
using Motive.AR.Media;
using Motive.Core.Scripting;
using Motive.Core.Models;
using Motive.AR.Models;
using Motive.Core;
#if MAPBOX_LATER
using Motive.Maps;
#endif
using Motive.Unity.Maps;
using Motive.AR.WeatherServices;

/// <summary>
/// Every game powered by Motive uses a Setup script that configures Motive
/// and starts the relevant services.
/// </summary>
public class GhostHunterSetup : ARSetup<GhostHunterSetup> {

    //public string ToolsCatalog;

    protected override void Start()
    {
        TaskManager.Instance.CreateLocationTaskDriver =
            (ctxt, task) => new GhostHunterLocationTaskDriver(ctxt, task);

        base.Start();

    }

}

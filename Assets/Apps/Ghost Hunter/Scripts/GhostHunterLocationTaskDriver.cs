﻿using Motive.AR.Models;
using Motive.Core.Scripting;
using Motive.Gaming.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostHunterLocationTaskDriver : LocationTaskDriver
{
    private ResourceActivationContext ctxt;
    private CharacterTask task;

    public override bool ShowInTaskPanel
    {
        get
        {
            // Don't show trap missions in the panel
            return Task.Action != "trap";
        }
    }

    public GhostHunterLocationTaskDriver(Motive.Core.Scripting.ResourceActivationContext ctxt, LocationTask task)
        : base(ctxt, task) { }
}

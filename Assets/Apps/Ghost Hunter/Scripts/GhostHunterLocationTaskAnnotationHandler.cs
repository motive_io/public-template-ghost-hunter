﻿using Motive.UI.Framework;
using Motive.Unity.Maps;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Cameras;
using T = Motive.Unity.Maps.MapAnnotation<LocationTaskDriver>;

public class GhostHunterLocationTaskAnnotationHandler : LocationTaskAnnotationHandler
{
    public AnnotationGameObject GhostTaskAnnotationObject;

    public GameObject DefaultGhostWorldObject;

    public override void SelectAnnotation(MapAnnotation annotation)
    {
        T typed = annotation as T;

        if (typed.Data.Task.Action == "trap")
        {
            PanelManager.Instance.Push<GhostHunterTrapPanel>(annotation);
        }
        else
        {
            base.SelectAnnotation(annotation);
        }
    }


    public override AnnotationGameObject GetObjectForAnnotation(MapAnnotation<LocationTaskDriver> annotation)
    {
        var task = annotation.Data.Task;

        AnnotationGameObject obj;

        if (task.Action == "trap")
        {
            Handheld.Vibrate();

            if (task.Marker != null || task.MarkerAssetInstance != null)
            {
                obj = GetObjectForAnnotation(annotation);
            }
            else
            {
                obj = Instantiate(GhostTaskAnnotationObject);
            }

            var lookAt = obj.GetComponent<LookatTarget>();

            if (lookAt)
            {
                lookAt.SetTarget(MapController.Instance.MapView.gameObject.transform);
            }
        }
        else
        {
            obj = base.GetObjectForAnnotation(annotation);
        }

        return obj;
    }
}

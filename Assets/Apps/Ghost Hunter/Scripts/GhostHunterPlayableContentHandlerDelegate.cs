using System;
using System.Collections;
using System.Collections.Generic;
using Motive.Core.Scripting;
using Motive.Gaming.Models;
using Motive.UI.Models;
using UnityEngine;
using LocalNotification = Motive.Core.Notifications.LocalNotification;
using Notification = Motive.UI.Models.Notification;

public class GhostHunterPlayableContentHandlerDelegate : DefaultPlayableContentHandlerDelegate
{

    protected override void PlayNotification(ResourceActivationContext context, PlayableContent playable, Notification notification, ResourcePanelData<Notification> data, Action onClose)
    {
        BackgroundNotifier.Instance.RemoveNotification(context.InstanceId);

        base.PlayNotification(context, playable, notification, data, onClose);
    }

    protected override void PushCharacterMessagePanel(ResourceActivationContext context, PlayableContent playable, CharacterMessage charMsg, ResourcePanelData<CharacterMessage> data, Action onClose)
    {
        try
        {
            BackgroundNotifier.Instance.RemoveNotification(context.InstanceId);

            if (charMsg.CharacterReference == null || charMsg.CharacterReference.Object == null)
                return;

            var charImageUrl = charMsg.CharacterReference.Object.ImageUrl;

            NotificationStore.Instance.Add(charMsg.Text, null, null, charImageUrl, charMsg, null);
            base.PushCharacterMessagePanel(context, playable, charMsg, data, onClose);
        }
        catch (Exception x)
        {
            m_logger.Exception(x);
        }
    }

    public override void StopPlaying(ResourceActivationContext ctxt, PlayableContent playable, bool interrupt = false)
    {
        BackgroundNotifier.Instance.RemoveNotification(ctxt.InstanceId);

        base.StopPlaying(ctxt, playable);
    }

    public override void Preview(ResourceActivationContext activationContext, PlayableContent playable, DateTime playTime)
    {
        if (playable.Content is CharacterMessage)
        {
            var charMsg = playable.Content as CharacterMessage;
            var character = CharacterDirectory.Instance.GetItem(charMsg.CharacterReference);
            var charName = character == null ? "" : character.Alias;

            var not = new LocalNotification
            {
                Title = charName,
                SendTime = playTime,
                Text = charMsg.Text,
                Image = character.ProfileImage
            };
            BackgroundNotifier.Instance.PutNotification(activationContext.InstanceId, not);
        }
        else if (playable.Content is Notification)
        {
            var notification = playable.Content as Notification;

            var not = new LocalNotification
            {
                Title = notification.Title,
                SendTime = playTime,
                Text = notification.Message
            };

            BackgroundNotifier.Instance.PutNotification(activationContext.InstanceId, not);
        }

        base.Preview(activationContext, playable, playTime);
    }

}

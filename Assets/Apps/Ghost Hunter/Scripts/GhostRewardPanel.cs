﻿using Motive.Gaming.Models;
using Motive.UI.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GhostRewardPanel : Panel<Collectible>
{
    public RawImage Image;
    public Text Title;

    public override void Populate(Collectible data)
    {
        Title.text = "YOU CAUGHT " + data.Title + "!";

        ImageLoader.LoadImageOnMainThread(data.ImageUrl, Image);

        base.Populate(data);
    }
}

﻿using Motive;
using UnityEditor;
using UnityEngine;

namespace Assets.Motive.Editor
{
    [CustomEditor(typeof(Platform))]
    public class MyScriptEditor : UnityEditor.Editor
    {
        override public void OnInspectorGUI()
        {
            /*
            var plat = target as Platform;
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Native Hooks", EditorStyles.boldLabel);
            plat.UseNativeDownloader = GUILayout.Toggle(plat.UseNativeDownloader, "Use Native Downloader");
            plat.EnableBackgroundLocation = GUILayout.Toggle(plat.EnableBackgroundLocation, "Enable Background Location");
            plat.EnableBackgroundAudio = GUILayout.Toggle(plat.EnableBackgroundAudio, "Enable Background Audio");
            plat.EnableBeacons = GUILayout.Toggle(plat.EnableBeacons, "Enable Beacons");
            plat.EnableNotifications = GUILayout.Toggle(plat.EnableNotifications, "Enable Notifications");

            if (plat.EnableNotifications)
                plat.EnableBackgroundNotifications =
                    GUILayout.Toggle(plat.EnableBackgroundNotifications, "Enable Background Notifications");
            */
            base.OnInspectorGUI();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Attractions.Models
{
    public enum AttractionRangeAutoplayBehavior
    {
        Never,
        Once,
        Always
    }
}

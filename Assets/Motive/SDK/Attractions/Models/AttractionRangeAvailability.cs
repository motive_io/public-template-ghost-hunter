﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Attractions.Models
{
    public enum AttractionRangeAvailability
    {
        Always,
        OnlyInRange,
        InRangeAndAfter
    }
}

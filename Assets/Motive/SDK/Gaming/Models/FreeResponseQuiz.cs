﻿using Motive.Core.Globalization;
using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
    public class FreeResponseQuiz : ScriptObject
    {
        public LocalizedText LocalizedTitle { get; set; }

        public LocalizedText LocalizedQuestion { get; set; }

        public LocalizedText LocalizedAnswer { get; set; }

        public string Title { get { return LocalizedText.GetText(LocalizedTitle); } }
        public string Question { get { return LocalizedText.GetText(LocalizedQuestion); } }
        public string Answer { get { return LocalizedText.GetText(LocalizedAnswer); } }
    }
}

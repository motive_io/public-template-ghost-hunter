﻿using UnityEngine;
using System.Collections;
using Motive.Core.Scripting;
using Motive.Core.Models;

namespace Motive.Gaming.Models
{
	public class CanCraftCollectibleCondition : AtomicCondition
	{
		public CollectibleCount CollectibleCount { get; set; }
	}
}


﻿using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
    public class ObjectiveCompleter : ScriptObject
    {
        public TaskObjective Objective { get; set; }
    }
}

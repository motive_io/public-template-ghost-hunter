﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
    public class EntityActionActivationOption
    {
        public ValuablesCollection Cost { get; set; }
        public ValuablesCollection Requirements { get; set; }
        public EntityActionAttributeValue[] Attributes { get; set; }
    }
}

﻿using Motive.Core.Models;
using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
    public class TaskObjectiveCompleteCondition : AtomicCondition
    {
        public ObjectReference TaskObjectiveReference { get; set; }
    }
}

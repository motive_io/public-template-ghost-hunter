﻿using Motive.Core.Scripting;
using Motive.Gaming.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
	public class WalletCondition : AtomicCondition
	{
	    public NumericalConditionOperator Operator { get; set; }
	    public CurrencyCount CurrencyCount { get; set; }
	}
}
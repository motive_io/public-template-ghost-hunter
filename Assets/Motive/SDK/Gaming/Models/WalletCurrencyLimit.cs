﻿using Motive.Core.Scripting;
using Motive.Gaming.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
	public class WalletCurrencyLimit : ScriptObject 
    {
		public NumericalOperator Operator { get; set; }
		public CurrencyCount[] CurrencyCounts { get; set; }
	}
}

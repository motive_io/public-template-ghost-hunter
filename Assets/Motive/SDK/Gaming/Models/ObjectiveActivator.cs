﻿using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
    public class ObjectiveActivator : ScriptObject
    {
        public TaskObjective Objective { get; set; }
    }
}

﻿using Motive.Core.Globalization;
using Motive.Core.Media;
using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
    public class MultipleChoiceQuizAnswer : QuizBase
    {
        public LocalizedText LocalizedAnswer { get; set; }

        public string Answer { get { return LocalizedText.GetText(LocalizedAnswer); } }
    }
}

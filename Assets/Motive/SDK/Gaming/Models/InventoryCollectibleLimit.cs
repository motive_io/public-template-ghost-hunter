﻿using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
    public class InventoryCollectibleLimit : ScriptObject
    {
        public NumericalOperator Operator { get; set; }
        public CollectibleCount[] CollectibleCounts { get; set; }
    }
}

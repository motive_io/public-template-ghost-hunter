﻿using UnityEngine;
using System.Collections;
using Motive.Core.Scripting;

namespace Motive.Gaming.Models
{
    public class PlayerReward : ScriptObject
    {
        public ValuablesCollection Reward { get; set; }
    }
}
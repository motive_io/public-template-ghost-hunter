﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
    public class EntityActionAttributeValue
    {
        public string Attribute { get; set; }
        public double Value { get; set; }
    }
}

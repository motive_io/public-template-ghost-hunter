﻿using UnityEngine;
using System.Collections;
using Motive.Core.Scripting;
using Motive.Core.Models;

namespace Motive.Gaming.Models
{
	public class RecipeActivatedCondition : AtomicCondition
	{
		public ObjectReference RecipeReference { get; set; }
	}
}


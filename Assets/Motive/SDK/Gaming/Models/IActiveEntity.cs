﻿using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
    public interface IActiveEntity : IScriptObject
    {
        EntityActionBehaviour[] EmittedActions { get; }
        EntityActionBehaviour[] ReceivedActions { get; }
    }
}

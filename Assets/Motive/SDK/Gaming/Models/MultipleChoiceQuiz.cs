﻿using Motive.Core.Globalization;
using Motive.Core.Media;
using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
    public class MultipleChoiceQuiz : QuizBase
    {
        public bool HasMutlipleAnswers { get; set; }

        public MultipleChoiceQuizAnswer[] Answers { get; set; }

        public override void GetMediaItems(IList<MediaItem> items)
        {
            if (Answers != null)
            {
                foreach (var answer in Answers)
                {
                    answer.GetMediaItems(items);
                }
            }

            base.GetMediaItems(items);
        }
    }
}

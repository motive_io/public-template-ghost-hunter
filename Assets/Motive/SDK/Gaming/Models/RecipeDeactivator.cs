﻿using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Motive.Core.Models;

namespace Motive.Gaming.Models
{
	public class RecipeDeactivator : ScriptObject
	{
		public ObjectReference[] RecipeReferences	 { get; set; }
	}
}


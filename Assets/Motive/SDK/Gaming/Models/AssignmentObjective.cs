﻿using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
    public class AssignmentObjective : ScriptObject
    {
        public TaskObjective Objective { get; set; }
        public bool IsOptional { get; set; }
        public bool IsHidden { get; set; }
    }
}

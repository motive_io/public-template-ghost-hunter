﻿using Motive.Core.Globalization;
using Motive.Core.Media;
using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Gaming.Models
{
    public class QuizBase : ScriptObject, IMediaItemProvider
    {
        public LocalizedMedia LocalizedMedia { get; set; }
        public LocalizedMedia LocalizedImage { get; set; }

        public string ImageUrl
        {
            get
            {
                return LocalizedMedia.GetMediaUrl(LocalizedImage);
            }
        }

        public MediaItem MediaItem
        {
            get
            {
                return LocalizedMedia.GetMediaItem(LocalizedMedia);
            }
        }

        public virtual void GetMediaItems(IList<Core.Media.MediaItem> items)
        {
            LocalizedMedia.GetMediaItems(LocalizedImage, items);
            LocalizedMedia.GetMediaItems(LocalizedMedia, items);
        }
    }
}

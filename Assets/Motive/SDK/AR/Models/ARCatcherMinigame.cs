﻿using Motive.Core.Scripting;
using Motive.Gaming.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.AR.Models
{
    public class ARCatcherMinigame : ScriptObject, ITaskMinigame
    {
        public bool HideMapAnnotation { get; set; }

		public LocationAugmentedOptions AROptions { get; set; }
    }
}

﻿using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.AR.Models
{
    public class ARLocationCollectionMechanic : ScriptObject, ILocationCollectionMechanic
    {
        public bool ShowMapAnnotation { get; set; }

		public LocationAugmentedOptions AROptions { get; set; }
    }
}

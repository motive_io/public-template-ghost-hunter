﻿using Motive.Core.Globalization;
using Motive.Core.Media;
using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Unity
{
    public class UnityAsset : Motive._3D.Models.BaseAsset, IMediaItemProvider
    {
        public AssetBundle AssetBundle { get; set; }
        public string AssetName { get; set; }

        public void GetMediaItems(IList<MediaItem> items)
        {
            if (AssetBundle != null)
            {
                AssetBundle.GetMediaItems(items);
            }
        }
    }
}

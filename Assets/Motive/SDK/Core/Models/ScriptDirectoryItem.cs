using Motive.Core.Globalization;
using Motive.Core.Media;
using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Core.Models
{
    public class ScriptDirectoryItem : ScriptObject, IMediaItemProvider
    {
        public ObjectReference ScriptReference { get; set; }

        public LocalizedText LocalizedTitle { get; set; }
        
        public int Order { get; set; }

        public TimeSpan? EstimatedDuration { get; set; }

        public PublishingStatus PublishingStatus { get; set; }

        public string ProductIdentifier { get; set; }
        public bool Autoplay { get; set; }

        public string Title
        {
            get
            {
                return LocalizedText.GetText(LocalizedTitle);
            }
        }

        public LocalizedText LocalizedDescription { get; set; }

        public string Description
        {
            get
            {
                return LocalizedText.GetText(LocalizedDescription);
            }
        }

        public LocalizedMedia LocalizedBackgroundImage { get; set; }

        public string BackgroundImageUrl
        {
            get
            {
                return LocalizedMedia.GetMediaUrl(LocalizedBackgroundImage);
            }
        }

        public void GetMediaItems(IList<MediaItem> items)
        {
            LocalizedMedia.GetMediaItems(LocalizedBackgroundImage, items);
        }
    }
}

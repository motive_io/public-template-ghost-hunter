﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Core.Models
{
    public enum PublishingStatus
    {
        Draft,
        Development,
        Published,
        Retired
    }
}

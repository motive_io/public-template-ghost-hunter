﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Motive.Core.Scripting;

namespace Motive.Core.Models
{
	public class AppVersionCondition : AtomicCondition 
	{
		public string Version { get; set; }
		public NumericalConditionOperator? Operator { get; set; }
	}
}

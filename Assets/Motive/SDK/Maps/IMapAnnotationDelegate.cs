﻿using Motive.UI.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Unity.Maps
{
    public interface IMapAnnotationDelegate
    {
        AnnotationGameObject GetObjectForAnnotation(MapAnnotation annotation);
        void SelectAnnotation(MapAnnotation annotation);
        void DeselectAnnotation(MapAnnotation annotation);
    }
}

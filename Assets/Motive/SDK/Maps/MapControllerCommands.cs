﻿using Motive.Unity.Maps;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapControllerCommands : MonoBehaviour
{
    public void CenterMap()
    {
        MapController.Instance.CenterMap();
    }
}

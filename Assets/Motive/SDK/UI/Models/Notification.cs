﻿using Motive.Core.Globalization;
using Motive.Core.Models;
using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Motive.Core.Media;

namespace Motive.UI.Models
{
    public class Notification : ScriptObject, IContent, IMediaItemProvider
    {
        public LocalizedText LocalizedTitle { get; set; }
        public LocalizedText LocalizedMessage { get; set; }
        public LocalizedMedia LocalizedSound { get; set; }
        public LocalizedMedia LocalizedImage { get; set; }
        public bool Vibrate { get; set; }

        public string Title
        {
            get
            {
                return LocalizedText.GetText(LocalizedTitle);
            }
        }

        public string Message
        {
            get
            {
                return LocalizedText.GetText(LocalizedMessage);
            }
        }

        public MediaItem Sound
        {
            get
            {
                return LocalizedMedia.GetMediaItem(LocalizedSound);
            }
        }

        public string SoundUrl
        {
            get
            {
                var sound = Sound;

                return sound != null ? sound.Url : null;
            }
        }

        public string ImageUrl
        {
            get
            {
                return LocalizedMedia.GetMediaUrl(LocalizedImage);
            }
        }

        public void GetMediaItems(IList<MediaItem> items)
        {
            LocalizedMedia.GetMediaItems(LocalizedSound, items);
        }
    }
}

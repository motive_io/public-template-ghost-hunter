﻿using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.UI.Models
{
    public class InterfaceDirector : ScriptObject
    {
        public ScriptObject[] Commands { get; set; }
    }
}

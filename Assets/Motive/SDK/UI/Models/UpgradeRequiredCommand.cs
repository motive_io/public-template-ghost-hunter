using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Motive.Core.Globalization;
using Motive.Core.Scripting;

namespace Motive.SDK.UI.Models
{
    public class UpgradeRequiredCommand : ScriptObject
    {
        public LocalizedText LocalizedMessage { get; set; }
        public LocalizedText LocalizedUpgradeButtonText { get; set; }

        public string AndroidStoreURL { get; set; }
        public string IOSStoreURL { get; set; }
    }
}

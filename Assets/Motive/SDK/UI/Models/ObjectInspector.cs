﻿using Motive.Core.Models;
using Motive.Core.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.UI.Models
{
    public class ObjectInspector : ScriptObject
    {
        public ObjectReference[] TargetReferences { get; set; }
        public IObjectInspectorContent Content { get; set; }
    }
}

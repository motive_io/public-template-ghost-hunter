﻿using Motive.Core.Globalization;
using UnityEngine;
using System.Collections;
using Motive.Core.Scripting;
using Motive.Core.Models;
using Motive.Core.Media;
using System.Collections.Generic;

namespace Motive.UI
{
	public class InterfaceUpdate : ScriptObject, IMediaItemProvider
    {
        public string Target { get; set; }
        public IContent Content { get; set; }

		public void GetMediaItems(IList<MediaItem> items)
		{
			var provider = Content as IMediaItemProvider;
				
			if (provider != null)
			{
				provider.GetMediaItems(items);
			}
		}
    }
}

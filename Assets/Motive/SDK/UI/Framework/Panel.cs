﻿
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace Motive.UI.Framework
{
    public class Panel<T> : Panel
    {
        public new T Data { get; private set; }

        public virtual void Populate(T data)
        {
        }

        public virtual void ClearData()
        {
        }

        public override void DidPush(object data)
        {
            m_dataObject = data;

            if (data is T)
            {
                Data = (T)data;
                Populate((T)data);

                IsReady = true;
            }
            else
            {
                Data = default(T);

                ClearData();
            }

            base.DidPush(data);
        }
    }

    public class Panel : MonoBehaviour
    {
        public bool PrePositioned;
        public bool ActiveAtStart;
        public bool Modal;
        public string StackName;

        public ScreenOrientation PreferredOrientation = ScreenOrientation.Unknown;

        public GameObject[] LinkedObjects;

		public GameObject AnimationTarget;
        public RuntimeAnimatorController PushAnimation;
        public RuntimeAnimatorController PopAnimation;
        public RuntimeAnimatorController ExitAnimation;

        public PanelContainer Container;

        public UnityEvent OnPush;
        public UnityEvent OnPop;

        public Action OnClose { get; set; }
        
        public bool IsReady { get; set; }

        public object Data
        {
            get
            {
                return m_dataObject;
            }
        }

        protected object m_dataObject;
		bool m_isActive;
        IEnumerable<PanelComponent> m_components;

        protected virtual void Awake()
        {
            if (!PrePositioned)
            {
                transform.localPosition = Vector2.zero;

                var rect = transform as RectTransform;

                if (rect)
                {
                    rect.anchoredPosition = Vector2.zero;
                }
            }

            m_components = GetComponents<PanelComponent>();

			SetActive(ActiveAtStart || m_isActive);
        }

		public virtual GameObject GetAnimationTarget()
		{
            return (AnimationTarget != null) ? AnimationTarget : this.gameObject;
		}

        public virtual void DoAnimation(RuntimeAnimatorController animController)
        {
            var tgt = GetAnimationTarget();

            var anim = tgt.GetComponent<Animator>();

            if (animController)
            {
                if (tgt)
                {
                    if (!anim)
                    {
                        anim = tgt.AddComponent<Animator>();
                    }

                    if (anim)
                    {
                        anim.runtimeAnimatorController = animController;
                    }
                }
            }
            else
            {
                if (anim)
                {
                    anim.runtimeAnimatorController = null;
                }
            }
        }

        public virtual void SetActive(bool active)
        {
			m_isActive = active;

            gameObject.SetActive(active);

            ObjectHelper.SetObjectsActive(LinkedObjects, active);
        }

        public virtual void Populate()
        {
            //IsReady = true;
        }

        public virtual void PopulateComponent<T>(object data)
            where T : PanelComponent
        {
            var component = GetComponent<T>();

            if (component != null)
            {
                component.Populate(data);
            }
        }

        public virtual void PopulateComponents(object obj)
        {
            if (m_components != null)
            {
                foreach (var c in m_components)
                {
                    c.DidShow(obj);
                }
            }
        }

        public virtual void DidShow()
        {
            PopulateComponents(this.m_dataObject);
        }

        public virtual void DidHide()
        {
            if (LinkedObjects != null)
            {
                foreach (var obj in LinkedObjects)
                {
					if (obj)
					{
						obj.SetActive(false);
					}
                }
            }

            if (m_components != null)
            {
                foreach (var c in m_components)
                {
                    c.DidHide();
                }
            }
        }

        public virtual void DidResignTop()
        {
        }

        public virtual void DidRegainTop()
        {
        }

        public virtual void DidPush()
        {
            Populate();

            if (OnPush != null)
            {
                OnPush.Invoke();
            }

			DidShow();
        }

        public virtual void DidPush(object data)
        {
            m_dataObject = data;

            DidPush();
        }

        public virtual void DidPop()
        {
            //gameObject.BroadcastMessage("DidHide", SendMessageOptions.DontRequireReceiver);
            DidHide();

            if (OnPop != null)
            {
                OnPop.Invoke();
            }
        }

        public virtual void Close()
        {
			PanelManager.Instance.Close(this);
        }

        public virtual void Back()
        {
            PanelManager.Instance.Pop(this);
        }

        public virtual void PushPanel(PanelLink link)
        {
            link.Push(m_dataObject);
        }

        public virtual void PushPanel(Panel p)
        {
            PanelManager.Instance.Push(p, m_dataObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Motive.UI.Framework
{
    public class PanelComponent : MonoBehaviour
    {
        public virtual void Populate()
        {

        }

        public virtual void Populate(object obj)
        {
            Populate();
        }

        public virtual void PopulateComponent<T>(object data)
            where T : PanelComponent
        {
            var component = GetComponent<T>();

            if (component != null)
            {
                component.Populate(data);
            }
        }

        public virtual void Close()
        {

        }

        public virtual void DidShow()
        {

        }

        public virtual void DidShow(object obj)
        {
            DidShow();
        }

        public virtual void DidHide()
        {
            Close();
        }
    }

    public class PanelComponent<T> : PanelComponent
    {
        public GameObject[] LinkedObjects;

        public T Data { get; protected set; }

        protected virtual void Awake()
        {
        }

        public virtual void Populate(T obj)
        {
        }

        public virtual void DidShow(T obj)
        {
            Data = obj;
            Populate(obj);
        }

		public virtual void ClearData()
		{
		}

        public override void Populate(object obj)
        {
            if (obj is T)
            {
                ObjectHelper.SetObjectsActive(LinkedObjects, true);

                Data = (T)obj;
                Populate((T)obj);
            }
            else
            {
                ObjectHelper.SetObjectsActive(LinkedObjects, false);

				ClearData();
            }

            base.Populate(obj);
        }

        public override void DidShow(object obj)
        {
            if (obj is T)
            {
                DidShow((T)obj);
            }
			else
			{
				ObjectHelper.SetObjectsActive(LinkedObjects, false);

				ClearData();
			}

            base.DidShow(obj);
        }

        public void SetImage(GameObject layout, RawImage rawImage, string url)
        {
            if (layout)
            {
                layout.SetActive(url != null);
            }

            ImageLoader.LoadImageOnThread(url, rawImage);
        }

        public void SetText(Text textField, string value)
        {
            SetText(null, textField, value);
        }

        public void SetText(GameObject layout, Text textField, string value)
        {
            if (textField)
            {
                textField.text = value;
            }

            if (layout)
            {
                layout.SetActive(!string.IsNullOrEmpty(value));
            }
        }

        public virtual void PushPanel(PanelLink link)
        {
            link.Push(Data);
        }

        public virtual void PushPanel(Panel p)
        {
            PanelManager.Instance.Push(p, Data);
        }
    }
}

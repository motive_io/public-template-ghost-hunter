﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeAreaTop : MonoBehaviour {
	//public RectTransform ReferenceRect;

	// Use this for initialization
	void Start () 
	{
		var tf = (RectTransform)this.transform;
		var duy = Screen.height - Screen.safeArea.height - Screen.safeArea.y;
        var topPct = duy / Screen.height;

        tf.anchorMax = new Vector2(1, 1);
        tf.anchorMin = new Vector2(0, 1 - topPct);

        tf.sizeDelta = new Vector2(0, 0);
	}
}

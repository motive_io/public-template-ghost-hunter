﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeAreaResize : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
		var tf = (RectTransform)this.transform;

		var dux = Screen.width - Screen.safeArea.width - Screen.safeArea.x;
		var duy = Screen.height - Screen.safeArea.height - Screen.safeArea.y;

        var left = Screen.safeArea.x / Screen.width;
        var right = (Screen.width - dux) / Screen.width;
        var bottom = Screen.safeArea.y / Screen.height;
        var top = (Screen.height - duy) / Screen.height;

        tf.anchorMin = new Vector2(left, bottom);
        tf.anchorMax = new Vector2(right, top);
        tf.sizeDelta = new Vector2(0, 0);
	}
}

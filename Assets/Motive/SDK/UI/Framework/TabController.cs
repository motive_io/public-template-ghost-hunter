﻿using UnityEngine;
using System.Collections;
using Motive.UI.Framework;

public class TabController : PanelContainer
{
    public TabControllerButton[] Tabs;
    public TabControllerButton SelectedTab;

    // Use this for initialization
    void Start()
    {
        Select(SelectedTab);
        StackBehavior = PanelStackBehavior.OneAtATime;
    }

    public void Select(TabControllerButton tab)
    {
        SelectedTab = tab;

        foreach (var obj in Tabs)
        {
            obj.SetSelected(obj == SelectedTab);
        }
    }
}

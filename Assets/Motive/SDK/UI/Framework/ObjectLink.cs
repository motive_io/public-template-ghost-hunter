﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectLink : MonoBehaviour {

    public string ObjectName;
    public GameObject GameObject;

    void Awake()
    {
        if (!GameObject && !string.IsNullOrEmpty(ObjectName))
        {
            GameObject = GameObject.Find(ObjectName);
        }
    }

    public void Call(string method)
    {
        if (GameObject)
        {
            GameObject.SendMessage(method, SendMessageOptions.DontRequireReceiver);
        }
    }

    public void SetActive(bool active)
    {
        if (GameObject)
        {
            GameObject.SetActive(active);
        }
    }
}

﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class Localize : LocalizeBase
{
    public Text Text;
    public string Default;

    public override void UpdateLocale()
    {
        if (!Text) return;

        var val = Localize.GetLocalizedString(this.localizationKey, Default);

        Text.text = val;
    }

    protected override void Start()
    {
        if (!Text)
        {
            Text = GetComponent<Text>();
        }

        if (Text && string.IsNullOrEmpty(Default))
        {
            Default = Text.text;
        }

        base.Start();
    }
}
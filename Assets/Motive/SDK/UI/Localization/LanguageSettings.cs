﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSettings : SingletonComponent<LanguageSettings>
{
    public SystemLanguage DefaultLanguage = SystemLanguage.English;

    // Use this for initialization
    void Start () 
    {
        LocalizeBase.SetCurrentLanguage(Locale.PlayerLanguage);
    }	
}

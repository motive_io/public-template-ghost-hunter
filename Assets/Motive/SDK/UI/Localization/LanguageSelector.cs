﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class LanguageSelector : MonoBehaviour
{
    public Dropdown dropdown;

    public void SetLanguage()
    {
        int value = dropdown.value;

        switch (value) {
            //case "English":
            case 0:
                Localize.SetCurrentLanguage(SystemLanguage.English);
                break;
            //case "Français":
            case 1:
                Localize.SetCurrentLanguage(SystemLanguage.French);
                break;
            default:
                Localize.SetCurrentLanguage(SystemLanguage.English);
                break;
        }
        
    }

    public void Start()
    {
        dropdown = GetComponent<Dropdown>();

        switch (Locale.PlayerLanguage.ToString())
        {
            case "English":
                dropdown.value = 0;
                break;
            case "French":
                dropdown.value = 1;
                break;
            default:
                dropdown.value = 0;
                break;
        }

    }




}

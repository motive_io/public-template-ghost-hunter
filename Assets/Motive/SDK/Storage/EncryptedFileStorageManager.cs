﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Core.Storage
{
    public class EncryptedFileStorageManager : FileStorageManager
    {
        public EncryptedFileStorageManager(string rootFolder) : base(rootFolder)
        {
        }

        public override IStorageAgent GetAgent(params string[] path)
        {
            return new EncryptedFileStorageAgent(EnsureFilePath(GetFileName(path)));
        }

        public override IStorageManager GetManager(params string[] path)
        {
            return new EncryptedFileStorageManager(EnsureFolder(m_rootFolder, path));
        }
    }
}

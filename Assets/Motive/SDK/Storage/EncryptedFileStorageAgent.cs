﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Motive.Core.Storage
{
    public class EncryptedFileStorageAgent : FileStorageAgent
    {
        public EncryptedFileStorageAgent(string fileName) : base(fileName)
        {

        }

        public override System.IO.Stream GetReadStream()
        {
            var baseStream = base.GetReadStream();

            if (baseStream != null)
            {
                var crypto = new DESCryptoServiceProvider()
                {
                    Key = Encoding.ASCII.GetBytes(Platform.Instance.EncryptionKey), 
                    IV = Encoding.ASCII.GetBytes(Platform.Instance.EncryptionIV) 
                };

                return new CryptoStream(baseStream, crypto.CreateDecryptor(), CryptoStreamMode.Read);
            }

            return null;
        }

        public override System.IO.Stream GetWriteStream()
        {
            var crypto = new DESCryptoServiceProvider()
            {
                Key = Encoding.ASCII.GetBytes(Platform.Instance.EncryptionKey), 
                IV = Encoding.ASCII.GetBytes(Platform.Instance.EncryptionIV) 
            };

            var baseStream = base.GetWriteStream();

            return new CryptoStream(baseStream, crypto.CreateEncryptor(), CryptoStreamMode.Write);
        }
    }
}

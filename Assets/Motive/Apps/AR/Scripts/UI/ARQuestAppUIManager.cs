﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Linq;
using Motive.Core.Models;

public class ARQuestAppUIManager : UIModeManager<ARQuestAppUIManager>
{
    public Mode CurrentMode { get; private set; }

    public Text TourNameText;
    public GameObject[] ActivateWhenRunning;

	public UnityEvent TourUpdated;

	public ScriptDirectoryItem RunningTour { get; private set; }

	public OptionsDialogPanel StopTourConfirm;

    public enum Mode
    {
        Map,
        AR
    }

    protected override void Awake()
    {
		if (TourUpdated == null)
		{
			TourUpdated = new UnityEvent();
		}

        AppManager.Instance.Initialized += (object sender, System.EventArgs e) =>
        {
            UpdateCurrentTour();

            ScriptRunnerManager.Instance.Updated += (_s, args) =>
            {
                UpdateCurrentTour();
            };
        };

        base.Awake();
    }

    protected override void Start()
    {
        DeactivateMode(Mode.AR);
        ActivateMode(Mode.Map);

        base.Start();
    }

    public void UpdateCurrentTour()
    {
        // should only be one
        var runningEpisodes = ScriptRunnerManager.Instance.GetRunningItems();

		RunningTour = runningEpisodes != null ? runningEpisodes.FirstOrDefault() : null;

		bool running = (RunningTour != null);

        if (TourNameText)
        {
            if (running)
            {
				TourNameText.text = RunningTour.Title.ToUpper();
            }
            else
            {
                TourNameText.text = null;
            }
        }

        if (ActivateWhenRunning != null)
        {
            foreach (var obj in ActivateWhenRunning)
            {
                if (obj)
                {
                    obj.SetActive(running);
                }
            }
        }

		if (TourUpdated != null)
		{
			TourUpdated.Invoke();
		}
    }

    public void StopCurrentTour()
    {
        // should only be one
        var runningEpisodes = ScriptRunnerManager.Instance.GetRunningItems();
        if (runningEpisodes == null || runningEpisodes.Count() > 1) return;

        var dirItem = runningEpisodes.FirstOrDefault();
        if (dirItem == null) return;

		if (StopTourConfirm)
		{
			StopTourConfirm.Show(new string[] { "stop", "cancel" }, (opt) =>
				{
					if (opt == "stop")
					{
						ScriptRunnerManager.Instance.Stop(dirItem);
					}
				});
		}
		else
		{
			ScriptRunnerManager.Instance.Stop(dirItem);
		}
    }

    public void SetMode(Mode mode)
    {
        if (CurrentMode != mode)
        {
            DeactivateMode(CurrentMode);
            ActivateMode(mode);
        }
    }

    public void ActivateMode(Mode mode)
    {
        CurrentMode = mode;

        switch (mode)
        {
            case Mode.AR:
                SetARActive(true);
                break;
            case Mode.Map:
                SetMapActive(true);
                break;
        }
    }

    public void DeactivateMode(Mode mode)
    {
        switch (mode)
        {
            case Mode.AR:
                SetARActive(false);
                break;
            case Mode.Map:
                SetMapActive(false);
                break;
        }
    }

    public override void SetARMode()
    {
        SetMode(Mode.AR);
    }

    public override void SetMapMode()
    {
        SetMode(Mode.Map);
    }
}


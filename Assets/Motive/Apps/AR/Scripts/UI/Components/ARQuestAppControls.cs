﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ARQuestAppControls : MonoBehaviour
{
	public UnityEvent TourUpdated;

	public Text TourName;

	public GameObject[] ActivateWhenRunning;
	public GameObject[] ActivateWhenNotRunning;

	void Awake()
	{
		if (TourUpdated == null)
		{
			TourUpdated = new UnityEvent();
		}
	}

	void Start()
	{
		ARQuestAppUIManager.Instance.TourUpdated.AddListener(OnTourUpdated);
	}

	void OnTourUpdated()
	{
		bool isRunning = ARQuestAppUIManager.Instance.RunningTour != null;
		
		if (TourName)
		{
			if (isRunning)
			{
				TourName.text = ARQuestAppUIManager.Instance.RunningTour.Title.ToUpper();
			}
			else
			{
				TourName.text = null;
			}
		}

		ObjectHelper.SetObjectsActive(ActivateWhenRunning, isRunning);
		ObjectHelper.SetObjectsActive(ActivateWhenNotRunning, !isRunning);

		if (TourUpdated != null)
		{
			TourUpdated.Invoke();
		}
	}

	public void StopCurrentTour()
	{
		ARQuestAppUIManager.Instance.StopCurrentTour();
	}

    public void SetARMode()
    {
        ARQuestAppUIManager.Instance.SetARMode();
    }

    public void SetMapMode()
    {
        ARQuestAppUIManager.Instance.SetMapMode();
    }
}

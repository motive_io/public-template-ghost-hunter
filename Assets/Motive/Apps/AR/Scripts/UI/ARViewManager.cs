﻿using Motive.Core.Utilities;
using Motive.Gaming.Models;
using Motive.UI.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARViewManager : SingletonComponent<ARViewManager> 
{
    public CollectibleTableItem InteractiveItem;
    public Table InteractiveItemTable;
    public Panel GuidePanel;
    public Panel TaskCompletePanel;

    public bool ShowTaskComplete;

    ListDictionary<string, CollectibleTableItem> m_interactiveCollectibles;

    protected override void Start()
    {
        m_interactiveCollectibles = new ListDictionary<string, CollectibleTableItem>();

        if (InteractiveItemTable)
        {
            InteractiveItemTable.Clear();
        }
    }

    public void AddInteractiveCollectible(string id, Collectible collectible, Action onSelect)
    {
        if (InteractiveItemTable && InteractiveItem)
        {
            var item = InteractiveItemTable.AddItem(InteractiveItem);

            item.Populate(collectible);

            item.OnSelected.AddListener(() => 
                {
                    onSelect();
                });

            m_interactiveCollectibles.Add(id, item);
        }
    }

    public void RemoveInteractiveCollectibles(string id)
    {
        if (InteractiveItemTable)
        {
            var items = m_interactiveCollectibles[id];

            if (items != null)
            {
                items.ForEach(i => InteractiveItemTable.RemoveItem(i));                
            }
        }
    }

    public void ClearGuide(ARGuideData guideData)
    {
        if (GuidePanel && GuidePanel.Data == guideData)
        {
            GuidePanel.Back();
        }
    }

    public void SetGuide(ARGuideData guideData)
    {
        if (GuidePanel)
        {
            if (guideData != null)
            {
                PanelManager.Instance.Push(GuidePanel, guideData);
            }
            else
            {
                GuidePanel.Back();
            }
        }
    }

    public void SetTaskComplete(PlayerTaskDriver taskDriver)
    {
        if (ShowTaskComplete && TaskCompletePanel)
        {
            PanelManager.Instance.Push(TaskCompletePanel, () =>
                {

                });
        }
    }
}

﻿using Motive.AR.LocationServices;
using Motive.AR.Models;
using Motive.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Motive.Unity.AR
{
    public class ARWorldAdapterBase : MonoBehaviour, IARWorldAdapter
    {
        public Camera WorldCamera;

		public Canvas Canvas;

		public GameObject[] ActivateWhenActive;

		public float DistanceScale = 1f;

        protected HashSet<ARWorldObject> m_worldObjects;

        public bool IsActive { get; private set; }

        protected virtual void Awake()
        {
            m_worldObjects = new HashSet<ARWorldObject>();
        }

		protected virtual void Start()
		{
		}

		public virtual void Initialize()
		{
			if (!WorldCamera)
			{
				WorldCamera = Camera.main;
			}
		}

        public virtual void Deactivate()
        {
			ObjectHelper.SetObjectsActive(ActivateWhenActive, false);

			if (Canvas)
			{
				Canvas.enabled = false;
			}

            IsActive = false;
        }

        public virtual void Activate()
        {
			ObjectHelper.SetObjectsActive(ActivateWhenActive, true);

			if (Canvas)
			{
				Canvas.enabled = true;
			}

            IsActive = true;
        }

        public virtual void RemoveWorldObject(ARWorldObject worldObject)
        {
            m_worldObjects.Remove(worldObject);
        }

        public virtual void AddWorldObject(ARWorldObject worldObject)
        {
            m_worldObjects.Add(worldObject);
        }

		protected virtual void AdjustObject(ARWorldObject obj)
		{
			double actualDistance;
			var dist = DistanceTo(obj, out actualDistance);

			if (obj.Options.VisibleRange != null)
			{
				if (!obj.Options.VisibleRange.IsInRange(actualDistance))
				{
					obj.GameObject.SetActive(false);
					return;
				}
			}

			obj.GameObject.SetActive(true);

			dist *= DistanceScale;

			var bearing = ForegroundPositionService.Instance.Position.GetBearingTo(obj.Coordinates);

			//var compass = m_heading;

			var angle = MathHelper.GetDegreesInRange(90 - bearing);

			Vector3 pos =
				new Vector3(
					(float)(MathHelper.CosDeg(angle) * dist),
					(float)obj.Elevation,
					(float)(MathHelper.SinDeg(angle) * dist));

			if (obj.Offset.HasValue)
			{
				pos.x += obj.Offset.Value.x;
				pos.y += obj.Offset.Value.y;
				pos.z += obj.Offset.Value.z;
			}

			obj.GameObject.transform.localPosition = pos;

			if (obj.Options != null && obj.Options.AlwaysFaceViewer)
			{
				obj.GameObject.transform.LookAt(this.gameObject.transform);
			}
		}

		protected Vector3 GetPosition(ARWorldObject obj, Coordinates relativeCoords = null)
		{
			//double actualDistance;
			var coords = relativeCoords ?? ForegroundPositionService.Instance.Position;

			var dist = coords.GetDistanceFrom(obj.Coordinates);
			var bearing = coords.GetBearingTo(obj.Coordinates);

			//var compass = m_heading;

			var angle = MathHelper.GetDegreesInRange(90 - bearing);

			// TODO: this isn't quite right
			Vector3 pos =
				new Vector3(
					(float)(MathHelper.CosDeg(angle) * dist),
					(float)obj.Elevation,
					(float)(MathHelper.SinDeg(angle) * dist));

			if (obj.Offset.HasValue)
			{
				pos.x += obj.Offset.Value.x;
				pos.y += obj.Offset.Value.y;
				pos.z += obj.Offset.Value.z;
			}

			return pos;
		}

        // Calculate actual and visual distance, taking into
		// account distance scaling and other options.
        protected double DistanceTo(ARWorldObject obj, out double actualDistance)
        {
            actualDistance = ForegroundPositionService.Instance.Position.GetDistanceFrom(obj.Coordinates);
            var visualDistance = actualDistance;

            // Default: z is north, x is east, y is up
            //
            if (obj.Options.DistanceVariation is FixedDistanceVariation)
            {
                var dfix = obj.Options.DistanceVariation as FixedDistanceVariation;

                visualDistance = dfix.Distance;
            }
            else
            {
                var dvar = obj.Options.DistanceVariation as LinearDistanceVariation;

                if (dvar != null)
                {
                    if (dvar.Range != null)
                    {
                        visualDistance = dvar.Range.Clamp(visualDistance);
                    }

                    if (dvar.Scale != null)
                    {
                        visualDistance = visualDistance * dvar.Scale.Value;
                    }
                }
            }

            return visualDistance;
        }

        public virtual float GetDistance(ARWorldObject worldObj)
        {
            return (float)ForegroundPositionService.Instance.Position.GetDistanceFrom(worldObj.Coordinates);
        }
    }
}

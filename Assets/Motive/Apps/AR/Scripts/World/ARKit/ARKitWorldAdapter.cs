﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Motive.AR.LocationServices;
using Motive.Core.Utilities;
using Motive.AR.Models;

#if MOTIVE_ARKIT
using UnityEngine.XR.iOS;
using Logger = Motive.Core.Diagnostics.Logger;
#endif

namespace Motive.Unity.AR
{
	public class ARKitWorldAdapter : ARWorldAdapterBase
	{
		public ARKitCameraManager CameraManager;
		public CameraGyroscope CameraGyro;

		public Text TrackingState;
		public Text RotationState;
		public Text UsingARKitText;
		public GameObject WorldAnchor;
		public GameObject DefaultAnchor;
		public GameObject ARKitAnchor;
		public Light ARKitLight;

        #if MOTIVE_ARKIT

		enum ObjectTrackingState
		{
			None,
			ARKit,
			Default
		}

		class WorldObjectState
		{
			public bool Added { get; set; }
			public ObjectTrackingState TrackingState { get; set; }
			public GameObject WorldObjectParent { get; set; }
		}

		Coordinates m_gpsAnchorCoords;
		UnityARUserAnchorData m_gpsAnchor;

		Logger m_logger;
		bool m_needsSetAnchor;
		bool m_usingARKit;

		Dictionary<ARWorldObject, WorldObjectState> m_objStates;

		protected override void Awake ()
		{
			base.Awake ();

			m_logger = new Logger(this);
			m_objStates = new Dictionary<ARWorldObject, WorldObjectState>();

			UnityARSessionNativeInterface.ARFrameUpdatedEvent += ARFrameUpdated;

			m_needsSetAnchor = true;

			//UnityARSessionNativeInterface.ARUserAnchorUpdatedEvent += HandleARUserAnchorUpdated;
		}

		public override void Initialize ()
		{
			base.Initialize ();

			if (!CameraGyro)
			{
				CameraGyro = WorldCamera.GetComponent<CameraGyroscope>();
			}

			CameraManager.Initialize();

			EnableARKit();
		}

        public static bool CheckSupportsARKit()
        {
            UnityARSessionNativeInterface.GetARSessionNativeInterface();
            return true;
        }

        void HandleARUserAnchorUpdated (ARUserAnchor anchorData)
		{
			if (anchorData.identifier == m_gpsAnchor.identifierStr)
			{
				//WorldAnchor.transform = anchorData.transform
			}
		}

		void AdjustARKitObject(ARWorldObject obj, WorldObjectState state)
		{
			if (obj.Options != null)
			{
				var distance = GetObjectDistance(state);

				if (obj.Options.VisibleRange != null &&
				   !obj.Options.VisibleRange.IsInRange(distance))

				{
					state.WorldObjectParent.SetActive(false);
					return;
				}

				state.WorldObjectParent.SetActive(true);

				double? fixedDistance = null;

				if (obj.Options.DistanceVariation is LinearDistanceVariation)
				{
					var distanceOpts = (LinearDistanceVariation)obj.Options.DistanceVariation;

					// Only thing to do is adjust distance scale if necessary
					if (DistanceScale != 1 || distanceOpts.Scale.GetValueOrDefault(1) != 1)
					{
						var scale = DistanceScale * distanceOpts.Scale.Value;

						// In the default case, we actually vary the distance placed by DistanceScale.
						// For ARKit, we achieve the same effect by varying the scale of the obejct
						// by the inverse computed scale.
						if (scale != 0)
						{
							state.WorldObjectParent.transform.localScale = Vector3.one / (float)distanceOpts.Scale.Value;
						}
					}

					if (distanceOpts.Range != null && !distanceOpts.Range.IsInRange(distance))
					{
						fixedDistance = distanceOpts.Range.Clamp(distance);
					}
				}
				else if (obj.Options.DistanceVariation is FixedDistanceVariation)
				{
					fixedDistance = ((FixedDistanceVariation)obj.Options.DistanceVariation).Distance;
				}

				if (fixedDistance.HasValue)
				{
					var camPos = Get2DPosition(WorldCamera.transform);
					var objPos = Get2DPosition(state.WorldObjectParent.transform);

					var deltaPos = camPos - objPos;

					var relativePos = deltaPos.normalized * (float)fixedDistance.Value;

					var worldPos = camPos - relativePos;
					worldPos.y = state.WorldObjectParent.transform.position.y;

					obj.GameObject.transform.position = worldPos;
				}
				else
				{
					obj.GameObject.transform.localPosition = Vector3.zero;
				}

				if (obj.Options.AlwaysFaceViewer)
				{
					obj.GameObject.transform.LookAt(this.gameObject.transform);
				}
			}
		}

		bool IsTracking(UnityARCamera camera)
		{
			// todo: is limited ok for tracking?
			return camera.trackingState == ARTrackingState.ARTrackingStateNormal ||
				camera.trackingState == ARTrackingState.ARTrackingStateLimited;
		}

		Vector3 GetObjectRelativePosition(Transform transform)
		{
			return new Vector3(
				WorldCamera.transform.position.x - transform.position.x,
				0,
				WorldCamera.transform.position.z - transform.position.z);
		}

		float GetObjectDistance(WorldObjectState state)
		{
			// If we're tracking in ARKit, use the computed 2d distance between the camera and
			// the object
			return GetObjectRelativePosition(state.WorldObjectParent.transform).magnitude;
		}

		private void SetObjectPosition(ARWorldObject worldObject, WorldObjectState state, Coordinates coords)
		{
			var pos = GetPosition(worldObject, coords);

			// Set the object to be level with the camera: may want to revisit 
			// this if we do ground surface tracking
			pos.y += 0;

			state.WorldObjectParent.transform.localPosition = pos;
			state.WorldObjectParent.transform.localScale = Vector3.one;
			state.WorldObjectParent.transform.localRotation = Quaternion.identity;
		}

		private void AdjustObject(ARWorldObject worldObject, UnityARCamera camera, WorldObjectState state)
		{
			state.WorldObjectParent.SetActive(true);

			bool isTracking = m_usingARKit && IsTracking(camera);

			var opts = worldObject.Options;

			if (!isTracking)
			{
				// Use the default rendering. We do this if ARKit stopped tracking.
				if (state.TrackingState != ObjectTrackingState.Default)
				{
					state.TrackingState = ObjectTrackingState.Default;

					state.WorldObjectParent.SetActive(false);

					worldObject.GameObject.transform.SetParent(DefaultAnchor.transform);

					worldObject.GameObject.transform.localRotation = Quaternion.identity;
					worldObject.GameObject.transform.localScale = Vector3.one;
				}

				AdjustObject(worldObject);
			}
			else
			{
				if (state.TrackingState != ObjectTrackingState.ARKit)
				{
					state.WorldObjectParent.transform.SetParent(ARKitAnchor.transform);

					worldObject.GameObject.transform.SetParent(state.WorldObjectParent.transform);

					worldObject.GameObject.transform.localPosition = Vector3.zero;
					worldObject.GameObject.transform.localScale = Vector3.one;
					worldObject.GameObject.transform.localRotation = Quaternion.identity;

					SetObjectPosition(worldObject, state, m_gpsAnchorCoords);

					state.TrackingState = ObjectTrackingState.ARKit;
				}

				AdjustARKitObject(worldObject, state);
			}
		}

		Vector2 GetMoveVector(Transform t1, Transform t2)
		{
			return new Vector2(t2.position.x - t1.position.x, t2.position.y - t1.position.y);
		}

		public void ARFrameUpdated(UnityARCamera camera)
		{
			/*
			if (!m_usingARKit)
			{
				return;
			}*/

			if (TrackingState)
			{
				TrackingState.text = camera.trackingState.ToString() + " " + m_resetCount + " " +
					ForegroundPositionService.Instance.Compass.TrueHeading;
			}

			if (ARKitLight)
			{
                ARKitLight.colorTemperature = camera.lightData.arLightEstimate.ambientColorTemperature;
                ARKitLight.intensity = camera.lightData.arLightEstimate.ambientIntensity / 1000f;
			}

			if (IsActive)
			{
				m_logger.Debug("Frame updated trackingState={0} gps anchor={1}",
					camera.trackingState, (m_gpsAnchorCoords == null)?"null":m_gpsAnchorCoords.ToString());
				
				bool isArkitTracking = IsTracking(camera);

				m_needsSetAnchor |= !isArkitTracking;

				if (m_usingARKit &&
					m_needsSetAnchor &&
					ForegroundPositionService.Instance.HasLocationData &&
					isArkitTracking)
				{
					SetAnchor();
					m_needsSetAnchor = false;

					// Reset all world objects?
				}

				if (isArkitTracking)
				{
					var move = GetMoveVector(WorldAnchor.transform, WorldCamera.transform);

					// ?
					var heading = Mathf.Atan2(move.x, move.y);

					// convert to gps?
					var currPos = m_gpsAnchorCoords.AddRadial(heading, move.magnitude);
				}

				foreach (var obj in m_worldObjects)
				{
					var state = m_objStates[obj];

					AdjustObject(obj, camera, state);
				}
			}
			/*
			Matrix4x4 matrix = new Matrix4x4();
			matrix.SetColumn(3, camera.worldTransform.column3);

			Vector3 currentPositon = UnityARMatrixOps.GetPosition(matrix) + (Camera.main.transform.forward * penDistance);
			if (Vector3.Distance (currentPositon, previousPosition) > minDistanceThreshold) {
				if (paintMode == 2) currentPaintVertices.Add (currentPositon);
				frameUpdated = true;
				previousPosition = currentPositon;
			}*/
		}

		public override void Activate ()
		{
			Recalibrate();
			
			CameraManager.Resume();

			WorldCamera.enabled = true;
			m_needsSetAnchor = true;

			base.Activate ();
		}

		public override void Deactivate ()
		{
			CameraManager.Pause();

			WorldCamera.enabled = false;

			base.Deactivate ();
		}

		public override void AddWorldObject (ARWorldObject worldObject)
		{
			var state = new WorldObjectState();

			var parent = new GameObject("AR Object");
			state.WorldObjectParent = parent;
			worldObject.GameObject.transform.SetParent(state.WorldObjectParent.transform);

			worldObject.GameObject.transform.localPosition = Vector2.zero;
			worldObject.GameObject.transform.localScale = Vector3.one;
			worldObject.GameObject.transform.rotation = Quaternion.identity;

			m_objStates.Add(worldObject, state);

			// Leave the object inactive until we parent it to the correct anchor.
			state.WorldObjectParent.SetActive(false);

			base.AddWorldObject (worldObject);
		}

		public override void RemoveWorldObject (ARWorldObject worldObject)
		{
			m_objStates.Remove(worldObject);

			base.RemoveWorldObject (worldObject);
		}

		int m_resetCount;

		float GetCompassRotation(Transform cameraTransform, Transform swivelTransform, double heading)
		{
			m_logger.Debug("Get rotation for cam=({0}) swivel=({1})",
				cameraTransform.rotation.eulerAngles, swivelTransform.rotation.eulerAngles);
			
			//// TODO: put this in a common space once I figure out wth it's doing
			/// 
			var globalCamFwd = cameraTransform.TransformPoint(Vector3.forward);
			var globalCamUp = cameraTransform.TransformPoint(Vector3.up);

			var swivelCamFwd = swivelTransform.InverseTransformPoint(globalCamFwd);
			var swivelCamUp = swivelTransform.InverseTransformPoint(globalCamUp);

			// Normalized forward projection in swivel space
			var camFwdProject = new Vector3(swivelCamFwd.x, swivelCamFwd.z).normalized;
			var camUpProject = new Vector3(swivelCamUp.x, swivelCamUp.z);

			var cross = Vector3.Cross(camFwdProject, camUpProject);

			var camHdg = Mathf.Atan2(swivelCamFwd.x, swivelCamFwd.z) * Mathf.Rad2Deg; 

			var camTilt = Mathf.Asin(cross.z) * Mathf.Rad2Deg;

			// Adjust heading based on tilt
			var hdgDiff = MathHelper.GetDegreesInRange(camHdg - heading);

			m_logger.Debug("hdgDiff={0} tilt={1}", hdgDiff, camTilt);
			
			var delta = hdgDiff - camTilt;

			return (float)delta;
			///////
		}

		public void Recalibrate()
		{
			SetAnchor();
		}

		Vector3 Get2DPosition(Transform transform)
		{
			return new Vector3(transform.position.x, 0, transform.position.z);
		}

		void MoveToCamera(Transform transform, bool resetHeight)
		{
			var y = resetHeight ? WorldCamera.transform.position.y : ARKitAnchor.transform.position.y;

			// X, Z come from World Camera, Y comes from World Anchor
			transform.position = 
				new Vector3(WorldCamera.transform.position.x, y, WorldCamera.transform.position.z);
		}

		void CalibrateCompass()
		{
			var heading = ForegroundPositionService.Instance.Compass.TrueHeading;

			var rotation = GetCompassRotation(WorldCamera.transform, WorldAnchor.transform, heading);

			if (RotationState)
			{
				RotationState.text = heading + " " + rotation;
			}

			m_logger.Debug("BEFORE: heading={0} rotation={1} pivot={2}", heading, rotation, WorldAnchor.transform.rotation.eulerAngles);

			WorldAnchor.transform.Rotate(new Vector3(0, rotation, 0));
			//WorldPivot.transform.RotateAround(Vector3.up, rotation);
			//WorldPivot.transform.localRotation = Quaternion.Euler(0, 
			//		rotation
			//		/*+ transform.localEulerAngles.z*/, 0);

			m_logger.Debug("AFTER: heading={0} rotation={1} pivot={2}", heading, rotation, WorldAnchor.transform.rotation.eulerAngles);
		}

		void SetAnchor()
		{
			m_resetCount++;

			MoveToCamera(WorldAnchor.transform, true);

			CalibrateCompass();

			m_gpsAnchorCoords = ForegroundPositionService.Instance.Position;
			m_gpsAnchor = UnityARSessionNativeInterface.GetARSessionNativeInterface().AddUserAnchorFromGameObject(WorldAnchor);

			// Reposition any ARKit objects so that they keep in sync with the GPS
			// position.
			foreach (var kv in m_objStates)
			{
				var state = kv.Value;
				var obj = kv.Key;

				if (state.TrackingState == ObjectTrackingState.ARKit)
				{
					state.TrackingState = ObjectTrackingState.None;

					SetObjectPosition(obj, state, m_gpsAnchorCoords);
				}
			}

			/*
			var currPose = Frame.Pose;

			var start = DateTime.Now;

			m_gpsAnchor = Session.CreateAnchor(currPose.position, currPose.rotation);
			m_gpsAnchorCoords = ForegroundPositionService.Instance.Position;

			var dt = DateTime.Now - start;

			DebugCameraPosition.text = "Create anchor took " + dt.TotalMilliseconds + "ms";

			m_logger.Debug("Setting anchor at {0} coords={1}", currPose.position, m_gpsAnchorCoords);
			*/
		}

		void EnableARKit()
		{
			CameraManager.EnableCameraTracking = true;
			CameraGyro.StopGyro();
			m_needsSetAnchor = true;

			m_usingARKit = true;
		}

		void DisableARKit()
		{
			CameraManager.EnableCameraTracking = false;
			CameraGyro.StartGyro();
			CalibrateCompass();

			MoveToCamera(WorldAnchor.transform, true);

			m_usingARKit = false;
		}

		public void SetUseARKit(bool useARKit)
		{
			if (useARKit == m_usingARKit)
			{
				return;
			}

			if (useARKit)
			{
				EnableARKit();
			}
			else
			{
				DisableARKit();
			}
		}

		public void ToggleUseARKit()
		{
			SetUseARKit(!m_usingARKit);
		}

		void Update()
		{
			if (RotationState)
			{
				RotationState.text = Platform.Instance.Compass.TrueHeading.ToString("F0");
			}

			if (UsingARKitText)
			{
				UsingARKitText.text = m_usingARKit ? "ARKit Active" : "ARKit Inactive";
			}
		}

        public override float GetDistance(ARWorldObject worldObj)
        {
            // TODO: if tracking..
            var dist = (worldObj.GameObject.transform.position - WorldCamera.transform.position).magnitude;

            if (worldObj.Options != null &&
                worldObj.Options.DistanceVariation is LinearDistanceVariation)
            {
                var linVar = (LinearDistanceVariation)worldObj.Options.DistanceVariation;

                dist /= (float)linVar.Scale.GetValueOrDefault(1);
            }

            return (float)dist;
        }
#endif
    }
}

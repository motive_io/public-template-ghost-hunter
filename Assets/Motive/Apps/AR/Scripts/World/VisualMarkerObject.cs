﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class VisualMarkerObject : MonoBehaviour, IPointerClickHandler
{
    public UnityEvent Clicked;

    public GameObject RenderObject;
    public GameObject LayoutObject;

    void Awake()
    {
        if (Clicked != null)
        {
            Clicked = new UnityEvent();
        }
    }

    public virtual void OnPointerClick(PointerEventData eventData)
    {
        if (Clicked != null)
        {
            Clicked.Invoke();
        }
    }
}

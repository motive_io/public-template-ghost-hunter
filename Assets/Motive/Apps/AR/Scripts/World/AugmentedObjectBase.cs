﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Motive.Unity.AR
{
    public class AugmentedObjectBase : MonoBehaviour, IPointerClickHandler
    {
        public ARWorldObject WorldObject { get; set; }

        public bool IsSelectable;

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            if (WorldObject != null)
            {
                WorldObject.OnClick();

                if (IsSelectable)
                {
                    ARWorld.Instance.Select(WorldObject);
                }
            }
        }

        public virtual void OnFocus()
        {
            if (WorldObject != null)
            {
                WorldObject.OnFocus();
            }
        }

        public virtual void OnFocusLost()
        {
            if (WorldObject != null)
            {
                WorldObject.OnFocusLost();
            }
        }

        public virtual void OnGazeEnter()
        {
            if (WorldObject != null)
            {
                WorldObject.OnGazeEnter();
            }
        }

        public virtual void OnGazeExit()
        {
            if (WorldObject != null)
            {
                WorldObject.OnGazeExit();
            }
        }
    }
}

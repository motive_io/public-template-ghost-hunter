﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Motive.Unity.AR
{
    public class AugmentedImageObject : AugmentedObjectBase
    {
        public GameObject RenderObject;
    }
}
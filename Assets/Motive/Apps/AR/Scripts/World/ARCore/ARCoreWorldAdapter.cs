﻿using Motive.AR.LocationServices;
using System;
using UnityEngine;
using UnityEngine.UI;
using Logger = Motive.Core.Diagnostics.Logger;

#if MOTIVE_ARCORE
using GoogleARCore;
using GoogleARCoreInternal;

namespace Motive.Unity.AR
{
    public class ARCoreWorldAdapter : ARWorldAdapterBase
    {
        Logger m_logger;
        Anchor m_gpsAnchor;
        Coordinates m_gpsAnchorCoords;

        public GameObject ARCamera;
        public GameObject WorldAnchor;

        public Text DebugTrackingState;
        public Text DebugAnchorPosition;
        public Text DebugCameraPosition;

        protected override void Awake()
        {
            m_logger = new Logger(this);
            ARCamera.SetActive(false);

            base.Awake();
        }

        public override void Activate()
        {
            ARCamera.SetActive(true);

            base.Activate();
        }

        public override void Deactivate()
        {
            ARCamera.SetActive(false);

            base.Deactivate();
        }

        public override void AddWorldObject(ARWorldObject worldObject)
        {
            base.AddWorldObject(worldObject);
            /*
            var anchor = Session.CreateAnchor(new Vector3(3, 0, 0), Quaternion.identity);
            //obj.transform.SetParent(anchor.transform);

            Motive.Unity.Timing.UnityTimer.Call(1, () =>
                {
                    m_logger.Debug("ANCHOR: state={0} pos={1}", anchor.TrackingState, anchor.transform.position);
                }, true);*/
        }

        public override void RemoveWorldObject(ARWorldObject worldObject)
        {
            base.RemoveWorldObject(worldObject);
        }

        int m_resetCount;
        bool m_needsReset;

        void SetAnchor()
        {
            m_needsReset = false;
            m_resetCount++;

            var currPose = Frame.Pose;

            var start = DateTime.Now;

            m_gpsAnchor = Session.CreateAnchor(currPose.position, currPose.rotation);
            m_gpsAnchorCoords = ForegroundPositionService.Instance.Position;

            var dt = DateTime.Now - start;

            DebugCameraPosition.text = "Create anchor took " + dt.TotalMilliseconds + "ms";

            m_logger.Debug("Setting anchor at {0} coords={1}", currPose.position, m_gpsAnchorCoords);
        }

        double m_lastTimestamp;

        void Update()
        {
            if (IsActive)
            {
                Pose junkPose;
                double timestamp;

                bool isTracking = SessionManager.Instance.MotionTrackingManager.TryGetLatestPose(out junkPose,
                    out timestamp);

                if (timestamp != m_lastTimestamp)
                {
                    m_lastTimestamp = timestamp;
                }

                var dt = m_lastTimestamp - timestamp;

                if (dt < 0.1)
                {
                    DebugAnchorPosition.text = dt.ToString();
                }
                else if (dt < 1)
                {
                    DebugAnchorPosition.text = "<color=yellow>" + dt + "</color>";
                }
                else
                {
                    DebugAnchorPosition.text = "<color=red>" + dt + "</color>";
                }

                if (Frame.TrackingState == FrameTrackingState.Tracking)
                {
                    if (m_needsReset ||
                        m_gpsAnchor == null ||
                        m_gpsAnchor.TrackingState != AnchorTrackingState.Tracking)
                    {
                        m_logger.Debug("No anchor or tracking lost, resetting");

                        // Re-establish anchor
                        SetAnchor();
                    }

                    m_logger.Debug("Anchor state={0} ({1})", m_gpsAnchor.TrackingState, m_resetCount);

                    if (DebugTrackingState)
                    {
                        DebugTrackingState.text = "Anchor: " + m_gpsAnchor.TrackingState.ToString() + " " + m_resetCount;
                    }

                    if (DebugAnchorPosition)
                    {
                        //DebugAnchorPosition.text = m_gpsAnchor.gameObject.transform.position.ToString();
                    }

                    if (DebugCameraPosition)
                    {
                        //DebugCameraPosition.text = MainCamera.transform.localPosition.ToString();
                    }

                    if (m_gpsAnchor.TrackingState == AnchorTrackingState.Tracking)
                    {
                        WorldAnchor.transform.localPosition = m_gpsAnchor.gameObject.transform.position;
                        WorldAnchor.transform.localRotation = m_gpsAnchor.gameObject.transform.rotation;

                        foreach (var obj in m_worldObjects)
                        {
                            AdjustAnchor(obj);
                        }
                    }
                    else
                    {
                        // Jump over to old approach
                    }
                }
            }
            else
            {
                m_needsReset = true;

                if (DebugTrackingState)
                {
                    DebugTrackingState.text = "<color=red>Frame: " + Frame.TrackingState + "</color>";
                }
            }
        }

        private void AdjustAnchor(ARWorldObject obj)
        {
            var angle = Math.Atan2(obj.Coordinates.Latitude, obj.Coordinates.Longitude);

            var dist = obj.Coordinates.GetDistanceFrom(m_gpsAnchorCoords);

            var pos = new Vector3(
                (float)(dist * Math.Cos(angle)),
                (float)(dist * Math.Sin(angle)));

            obj.GameObject.transform.SetParent(WorldAnchor.transform);
            obj.GameObject.transform.localPosition = m_gpsAnchor.transform.position + pos;

            m_logger.Debug("gps anchor at {0}, object at {1}, camera at {2}",
                m_gpsAnchor.transform.position,
                obj.GameObject.transform.position,
                ARCamera.transform.position);
        }
    }
}
#endif
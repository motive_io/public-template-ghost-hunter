﻿using Motive.AR.LocationServices;
using Motive.AR.Models;
using Motive.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Motive.Unity.AR
{
    public class DefaultWorldAdapter : ARWorldAdapterBase
    {
        private bool m_doUpdate;

        public CameraGyroscope GyroCamera;
        public GameObject Cameras;
        public GameObject WorldAnchor;
        public WebCamPreview BackgroundTexturePreview;

        protected Logger m_logger;

        protected override void Awake()
        {
            if (!GyroCamera)
            {
                GyroCamera = GetComponentInChildren<CameraGyroscope>();
            }

            base.Awake();

			Deactivate();
        }

        public override void Activate()
        {
            m_doUpdate = true;
            Cameras.SetActive(true);
            WorldAnchor.SetActive(true);

            if (GyroCamera)
            {
                GyroCamera.CalibrateCompass();
            }

            if (BackgroundTexturePreview)
            {
                BackgroundTexturePreview.StartCamera();
            }
        }

        public override void Deactivate()
        {
            Cameras.SetActive(false);
            WorldAnchor.SetActive(false);

            if (BackgroundTexturePreview)
            {
                BackgroundTexturePreview.StopCamera();
            }
        }

        public override void AddWorldObject(ARWorldObject worldObject)
        {
            GameObject parent = WorldAnchor;

            /*
            if (!m_objectGroups.TryGetValue(groupName, out parent))
            {
                parent = AddObjectGroup(groupName);
            }*/

            worldObject.GameObject.layer = this.gameObject.layer;
            worldObject.GameObject.transform.SetParent(parent.transform);

            SetPosition(worldObject); 
            
            base.AddWorldObject(worldObject);
        }

        private void SetPosition(ARWorldObject obj)
        {
            double actualDistance;
            var dist = DistanceTo(obj, out actualDistance);

            if (obj.Options.VisibleRange != null)
            {
                if (!obj.Options.VisibleRange.IsInRange(actualDistance))
                {
                    obj.GameObject.SetActive(false);
                    return;
                }
            }

            obj.GameObject.SetActive(true);

            dist *= DistanceScale;

            var bearing = ForegroundPositionService.Instance.Position.GetBearingTo(obj.Coordinates);

            //var compass = m_heading;

            var angle = MathHelper.GetDegreesInRange(90 - bearing);

            Vector3 pos =
                new Vector3(
                    (float)(MathHelper.CosDeg(angle) * dist),
                    (float)obj.Elevation,
                    (float)(MathHelper.SinDeg(angle) * dist));

            if (obj.Offset.HasValue)
            {
                pos.x += obj.Offset.Value.x;
                pos.y += obj.Offset.Value.y;
                pos.z += obj.Offset.Value.z;
            }

            obj.GameObject.transform.localPosition = pos;

            if (obj.Options != null && obj.Options.AlwaysFaceViewer)
            {
                obj.GameObject.transform.LookAt(this.gameObject.transform);
            }
        }

        void Update()
        {
            if (m_doUpdate)
            {
                foreach (var obj in m_worldObjects)
                {
                    SetPosition(obj);
                }
            }
        }
    }
}

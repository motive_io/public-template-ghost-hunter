﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Motive.Unity.AR
{
    public interface IARWorldAdapter
    {
        void Deactivate();

        void Activate();

        void RemoveWorldObject(ARWorldObject worldObject);

        void AddWorldObject(ARWorldObject worldObject);
    }
}

using UnityEngine;
using System.Collections;
using Motive.AR.LocationServices;
using System.Collections.Generic;
using System;
using Motive.Core.Utilities;
using Motive.Core.Models;
using Motive.AR.Models;
using Motive.Core.Scripting;
using Motive.Unity.UI;
using Motive.Unity.Utilities;
using Logger = Motive.Core.Diagnostics.Logger;
using System.Linq;
using Motive.SDK.Camera;
using Motive.Unity;
using UnityEngine.Events;

#if MOTIVE_VUFORIA
using Motive.AR.Vuforia;
#endif

namespace Motive.Unity.AR
{
	/// <summary>
	/// This clas manages Location-AR objects. Uses adapters to handle
	/// positioning and cameras.
	/// </summary>
    public class ARWorld : SingletonComponent<ARWorld>
    {
        class ARFence
        {
            public string RequestId { get; set; }
            public ARWorldObject WorldObject { get; set; }
            public DoubleRange Range { get; set; }
            public Action OnInRange { get; set; }
            public Action OnOutOfRange { get; set; }

            public bool? IsInRange { get; set; }
        }

        public bool IsActive { get; private set; }

        public UnityEvent OnUpdated;

        //private bool m_doUpdate;
        private SetDictionary<ARWorldObject, ARFence> m_worldObjectFences;
        private HashSet<ARWorldObject> m_worldObjects;
        private SetDictionary<string, ARWorldObject> m_worldObjDictionary;
        private Dictionary<string, GameObject> m_objectGroups;

        public AugmentedImageObject AugmentedImageObject;
        public Augmented3dAssetObject Augmented3dObject;
        public AugmentedVideoObject AugmentedVideoObject;
        public AugmentedAudioObject AugmentedAudioObject;

        public ARWorldAdapterBase DefaultAdapter;
        public ARWorldAdapterBase ARCoreAdapter;
        public ARWorldAdapterBase ARKitAdapter;

        // radius to interact with nodes
        public float DistanceScale = 1f;
        public float DefaultFixedDistance = 5f;

		public bool ActiveOnWake;

        public ARWorldObject SelectedObject { get; private set; }

        private AugmentedObjectBase m_gazeObject;
        private AugmentedObjectBase m_focusObject;

        protected Logger m_logger;

        public Camera MainCamera
        { 
            get 
            {
                return ARAdapter ? ARAdapter.WorldCamera : null;
            } 
        }

        public ARWorldAdapterBase ARAdapter;

        public void Activate()
        {
            IsActive = true;

            ARAdapter.Activate();

#if MOTIVE_VUFORIA
		VuforiaWorld.Instance.Activate();
#endif
        }

        public void Deactivate()
		{
            IsActive = false;

            ARAdapter.Deactivate();

            //m_doUpdate = false;

#if MOTIVE_VUFORIA
		VuforiaWorld.Instance.Deactivate();
#endif
        }

        protected override void Awake()
        {
            base.Awake();

            if (OnUpdated == null)
            {
                OnUpdated = new UnityEvent();
            }

            if (!ARAdapter)
            {
                if (UnityEngine.Application.isMobilePlatform)
                {
#if UNITY_ANDROID && MOTIVE_ARCORE
                    if (ARCoreAdapter)
                    {
						// TODO: is ARCore supported on this device?
                        ARAdapter = Instantiate(ARCoreAdapter);
                    }
                    else
                    {
                        ARAdapter = Instantiate(DefaultAdapter);
                    }
#endif
#if UNITY_IOS && MOTIVE_ARKIT
                    if (ARKitAdapter && ARKitHelper.IsSupported())
					{
						ARAdapter = Instantiate(ARKitAdapter);
					}
					else
					{
						ARAdapter = Instantiate(DefaultAdapter);
					}
#endif
                }

                if (!ARAdapter && DefaultAdapter)
                {
                    ARAdapter = Instantiate(DefaultAdapter);
                }
            }

            if (ARAdapter != null)
            {
                ARAdapter.transform.SetParent(this.gameObject.transform);
                ARAdapter.transform.localPosition = Vector3.zero;
            }

            m_logger = new Logger(this);
            m_worldObjects = new HashSet<ARWorldObject>();
            m_worldObjDictionary = new SetDictionary<string, ARWorldObject>();
            m_objectGroups = new Dictionary<string, GameObject>();
            m_worldObjectFences = new SetDictionary<ARWorldObject, ARFence>();

            //AddObjectGroup("default");

            //GetComponentInChildren<Renderer>().material.mainTexture = CameraManager.Instance._camera.LivePreview.texture;
        }

		protected override void Start ()
		{
			ARAdapter.Initialize();

			if (ActiveOnWake)
			{
				ARAdapter.Activate();
			}
			else
			{
				ARAdapter.Deactivate();
			}

			base.Start ();
		}

        /*
        GameObject AddObjectGroup(string name)
        {
            var objGroup = new GameObject(name);
            objGroup.transform.SetParent(WorldAnchor.transform);

            objGroup.transform.localPosition = Vector3.zero;
            objGroup.transform.localRotation = Quaternion.identity;
            objGroup.transform.localScale = Vector3.one;

            m_objectGroups[name] = objGroup;

            return objGroup;
        }*/

        public LocationAugmentedOptions GetDefaultImageOptions(DoubleRange visibleRange = null)
        {
            return LocationAugmentedOptions.GetLinearDistanceOptions(true, visibleRange);
            //return LocationAugmentedOptions.GetFixedDistanceOptions(DefaultFixedDistance, true, visibleRange);
        }

        public void RemoveWorldObject(ARWorldObject worldObject)
        {
            m_logger.Debug("Remove world object");

            if (m_worldObjects.Contains(worldObject))
            {
                m_worldObjects.Remove(worldObject);
            }

            ARAdapter.RemoveWorldObject(worldObject);

            if (!worldObject.GameObject) return;

            worldObject.GameObject.transform.SetParent(null);

            Destroy(worldObject.GameObject);
        }

        public void CreateFence(
            string requestId, 
            ARWorldObject worldObj,
            DoubleRange range,
            Action onInRange,
            Action onOutOfRange)
        {
            var fence = new ARFence
            {
                RequestId = requestId,
                WorldObject = worldObj,
                Range = range,
                OnInRange = onInRange,
                OnOutOfRange = onOutOfRange
            };

            m_worldObjectFences.Add(worldObj, fence);
        }

        public void DiscardFence(ARWorldObject worldObj, string requestId)
        {
            m_worldObjectFences.RemoveWhere(worldObj, f => f.RequestId == requestId);
        }

        public IEnumerable<ARWorldObject> GetWorldObjects(string instanceId)
        {
            return m_worldObjDictionary[instanceId];
        }

        public ARWorldObject GetObject(string instanceId, Location location)
        {
            var objs = GetWorldObjects(instanceId);

            if (objs != null)
            {
                return objs.Where(o => o.Location == location)
                    .FirstOrDefault();
            }

            return null;
        }

        public ARWorldObject GetNearestObject(string instanceId)
        {
            var objs = m_worldObjDictionary[instanceId];

            if (objs != null)
            {
                if (ForegroundPositionService.Instance.HasLocationData)
                {
                    var coords = ForegroundPositionService.Instance.Position;

                    return objs
                        .OrderBy(o => coords.GetDistanceFrom(o.Location.Coordinates))
                        .FirstOrDefault();
                }
                else
                {
                    return objs.FirstOrDefault();
                }
            }

            return null;
        }

        public void AddWorldObject(ARWorldObject worldObject, string groupName = "default")
        {
            m_worldObjects.Add(worldObject);

            ARAdapter.AddWorldObject(worldObject);

            /****
            GameObject parent = null;

            if (!m_objectGroups.TryGetValue(groupName, out parent))
            {
                parent = AddObjectGroup(groupName);
            }

            worldObject.GameObject.transform.SetParent(parent.transform);

            SetPosition(worldObject);
             */
        }

        public Augmented3dAssetObject AddAugmentedAsset(Location location, Motive._3D.Models.AssetInstance assetInstance, ILocationAugmentedOptions options)
        {
            if (assetInstance == null || assetInstance.Asset == null)
            {
                return null;
            }

            var augmentedObject = Instantiate(Augmented3dObject);

            var worldObj = new ARWorldObject
            {
                Location = location,
                GameObject = augmentedObject.gameObject,
                Options = options
            };

            augmentedObject.WorldObject = worldObj;

            ThreadHelper.Instance.StartCoroutine(
                UnityAssetLoader.LoadAsset<GameObject>(assetInstance.Asset as UnityAsset, (assetObj) =>
                {
                    // augmentedObj could have been desstroyed here
                    if (assetObj != null && augmentedObject)
                    {
                        ObjectHelper.InstantiateAsset(assetObj, assetInstance, augmentedObject.transform);

                        AddWorldObject(worldObj);
                    }
                }));

            return augmentedObject;
        }

		public AugmentedImageObject AddAugmentedImage(Location location, string url, ILocationAugmentedOptions options, Motive.Core.Models.Color color = null)
        {
            var obj = Instantiate(AugmentedImageObject);

            ThreadHelper.Instance.StartCoroutine(ImageLoader.LoadImage(url, obj.RenderObject));

            var worldObj = new ARWorldObject
            {
                Location = location,
                GameObject = obj.gameObject,
                Options = options,
            };

            obj.WorldObject = worldObj;

			if (color != null)
			{
				var renderer = obj.RenderObject.GetComponent<Renderer>();

				if (renderer)
				{
					renderer.material.color = ColorHelper.ToUnityColor(color);
				}
			}

            AddWorldObject(obj.WorldObject);

            return obj;
        }

        internal void RemoveAugmentedMarker(ResourceActivationContext ctxt, LocationAugmentedImage resource)
        {
            RemoveResourceObjects(ctxt.InstanceId);
        }

        void SetUpAugmentedObject(AugmentedObjectBase obj, ResourceActivationContext context)
        {
            obj.WorldObject.Clicked += (sender, args) =>
            {
                context.FireEvent("select");

                ObjectInspectorManager.Instance.Select(context);
            };

            obj.WorldObject.GazeEntered += (sender, args) =>
            {
                context.FireEvent("gaze");
                context.SetState("gazing");

                //ObjectInspectorManager.Instance.ObjectAction(context, "gaze");
            };

            obj.WorldObject.GazeExited += (sender, args) =>
            {
                context.ClearState("gazing");
                //ObjectInspectorManager.Instance.EndObjectAction(context, "gaze");
            };

            obj.WorldObject.Focused += (sender, args) =>
            {
                context.FireEvent("focus");
                context.SetState("in_focus");

                ObjectInspectorManager.Instance.ObjectAction(context, "focus");
            };

            obj.WorldObject.FocusLost += (sender, args) =>
            {
                context.ClearState("in_focus");
                ObjectInspectorManager.Instance.EndObjectAction(context, "focus");
            };

            obj.WorldObject.EnteredView += (sender, args) =>
            {
                context.SetState("visible");
                ObjectInspectorManager.Instance.SetObjectAvailable(context, true);
            };

            obj.WorldObject.ExitedView += (sender, args) =>
            {
                context.ClearState("visible");
                ObjectInspectorManager.Instance.SetObjectAvailable(context, false);
            };

            m_worldObjDictionary.Add(context.InstanceId, obj.WorldObject);

            OnUpdated.Invoke();
        }

        internal void AddAugmentedMarker(ResourceActivationContext context, LocationAugmentedImage resource, IEnumerable<Location> locations = null)
        {
            locations = locations ?? resource.Locations;

            if (resource.Marker == null || locations == null)
            {
                return;
            }

            foreach (var loc in locations)
            {
				var obj = AddAugmentedImage(loc, resource.Marker.MediaUrl, resource, resource.Marker.Color);

                if (resource.Marker.Layout != null)
                {
                    LayoutHelper.Apply(obj.RenderObject.transform, resource.Marker.Layout);
                }

                SetUpAugmentedObject(obj, context);
            }
        }

        internal void Remove3dAsset(string instanceId)
        {
            RemoveResourceObjects(instanceId);
        }

        void RemoveResourceObjects(string instanceId)
        {
            var objs = m_worldObjDictionary[instanceId];

            if (objs != null)
            {
                foreach (var wo in objs)
                {
                    RemoveWorldObject(wo);
                }
            }

            m_worldObjDictionary.RemoveAll(instanceId);

            OnUpdated.Invoke();
        }

        public void ShowOnly(params string[] groups)
        {
            foreach (var kv in m_objectGroups)
            {
                kv.Value.SetActive(groups.Contains(kv.Key));
            }
        }

        public void ShowAllObjects()
        {
            foreach (var obj in m_objectGroups.Values)
            {

                obj.SetActive(true);

            }

        }

        internal void HideAllObjects()
        {
            foreach (var obj in m_objectGroups.Values)
            {
                obj.SetActive(false);
            }
        }

        internal void Add3dAsset(ResourceActivationContext context, LocationAugmented3DAsset resource, IEnumerable<Location> locations = null)
        {
            locations = locations ?? resource.Locations;

            if (resource.AssetInstance == null ||
                resource.AssetInstance.Asset == null ||
                locations == null)
            {
                return;
            }

            ThreadHelper.Instance.StartCoroutine(
                UnityAssetLoader.LoadAsset<GameObject>(resource.AssetInstance.Asset as UnityAsset, (assetObj) =>
                {
                    if (assetObj != null)
                    {
                        foreach (var loc in locations)
                        {
                            var obj = Instantiate(Augmented3dObject);

                            ObjectHelper.InstantiateAsset(assetObj, resource.AssetInstance, obj.transform);

                            var worldObj = new ARWorldObject
                            {
                                Location = loc,
                                GameObject = obj.gameObject,
                                Options = resource
                            };

                            obj.WorldObject = worldObj;

                            worldObj.Clicked += (sender, args) =>
                            {
                                context.FireEvent("select");

                                ObjectInspectorManager.Instance.Select(context);
                            };

                            AddWorldObject(worldObj);

                            SetUpAugmentedObject(obj, context);
                        }
                    }
                }));
        }

        bool IsCloser(Vector3 pos, Vector3 comparedTo)
        {
            var d1 = new Vector2(pos.x - .5f, pos.y - .5f).sqrMagnitude;
            var d2 = new Vector2(comparedTo.x - .5f, comparedTo.y - .5f).sqrMagnitude;

            return d1 < d2;
        }

        bool IsVisible(Vector3 pos)
        {
            return pos.z > 0 && (pos.x >= 0 && pos.x <= 1) && (pos.y >= 0 && pos.y <= 1);
        }

        public Vector3 GetScreenPosition(ARWorldObject worldObject)
        {
            if (worldObject.GameObject != null)
            {
                var pos =
                    ARWorld.Instance.MainCamera.WorldToViewportPoint(worldObject.GameObject.transform.position);

                return pos;
            }

            return new Vector3(-1, -1, -1);
        }

        // Update is called once per frame
        void Update()
        {
            /*
            if (m_doUpdate)
            {
                foreach (var obj in m_worldObjects)
                {
                    SetPosition(obj);
                }
            }*/

            if (!MainCamera)
            {
                return;
            }

            // Find out what's in focus

            var w = Screen.width / 2;
            var h = Screen.height / 2;

            var ray = MainCamera.ScreenPointToRay(new Vector2(w, h));

            RaycastHit hitInfo;

            AugmentedObjectBase gazeObj = null;
            AugmentedObjectBase focusObj = null;

            if (Physics.Raycast(ray, out hitInfo))
            {
                gazeObj = hitInfo.collider.gameObject.GetComponentInParent<AugmentedObjectBase>();
                focusObj = gazeObj;
            }

            GameObject nearest = null;
            Vector3 nearestPos = Vector3.zero;

            // Find out if anything is on screen, choose the closest to middle
            foreach (var obj in m_worldObjects)
            {
                if (obj.GameObject)
                {
                    var pos = MainCamera.WorldToViewportPoint(obj.GameObject.transform.position);

                    var vis = IsVisible(pos);

                    obj.SetVisible(vis);

                    // Focus object is the gaze object or next-nearest object.
                    // If we already have a focus object, don't bother trying to
                    // compute the next nearest one.
                    if (vis && !focusObj)
                    {
                        if (nearest == null || IsCloser(pos, nearestPos))
                        {
                            nearest = obj.GameObject;
                            nearestPos = pos;
                        }
                    }

                    var fences = m_worldObjectFences[obj];

                    if (fences != null)
                    {
                        var d = GetDistance(obj);

                        foreach (var fence in fences)
                        {
                            if (fence.Range != null)
                            {
                                if (fence.Range.IsInRange(d) && obj.GameObject.activeSelf)
                                {
                                    // if "IsInRange" isn't set OR
                                    // fence is not currently in range
                                    // then signal "on in range"
                                    if (!fence.IsInRange.HasValue ||
                                        !fence.IsInRange.Value)
                                    {
                                        fence.IsInRange = true;

                                        if (fence.OnInRange != null)
                                        {
                                            fence.OnInRange();
                                        }
                                    }
                                }
                                else
                                {
                                    // if "IsInRange" isn't set OR
                                    // fence is currently in range
                                    // then signal "on out of range"
                                    if (!fence.IsInRange.HasValue ||
                                        fence.IsInRange.Value)
                                    {
                                        fence.IsInRange = false;

                                        if (fence.OnOutOfRange != null)
                                        {
                                            fence.OnOutOfRange();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (nearest)
            {
                focusObj = nearest.GetComponentInParent<AugmentedObjectBase>();
            }

            if (m_gazeObject != null)
            {
                if (gazeObj != m_gazeObject)
                {
                    m_gazeObject.OnGazeExit();
                    m_gazeObject = null;

                    if (gazeObj)
                    {
                        m_gazeObject = gazeObj;
                        m_gazeObject.OnGazeEnter();
                    }
                }
            }
            else if (gazeObj)
            {
                m_gazeObject = gazeObj;
                m_gazeObject.OnGazeEnter();
            }

            if (m_focusObject != null)
            {
                if (focusObj != m_focusObject)
                {
                    m_focusObject.OnFocusLost();
                    m_focusObject = null;

                    if (focusObj)
                    {
                        m_focusObject = focusObj;
                        m_focusObject.OnFocus();
                    }
                }
            }
            else if (focusObj)
            {
                m_focusObject = focusObj;
                m_focusObject.OnFocus();
            }
        }

        public void Select(ARWorldObject obj)
        {
            var lastSelected = SelectedObject;
            SelectedObject = obj;

            if (lastSelected != obj)
            {
                if (obj != null)
                {
                    obj.OnSelect();
                }

                if (lastSelected != null)
                {
                    lastSelected.OnDeselect();
                }
            }
        }

        internal float GetDistance(ARWorldObject worldObj)
        {
            // TODO: should defer to the adapter
            return ARAdapter.GetDistance(worldObj);
        }
    }
}

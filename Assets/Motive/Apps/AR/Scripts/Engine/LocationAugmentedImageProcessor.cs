﻿using Motive.AR.Models;
using Motive.Unity.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Motive.Core.Scripting;
using Motive.Unity.AR;

public class LocationAugmentedImageProcessor : ThreadSafeScriptResourceProcessor<LocationAugmentedImage>
{
    public override void ActivateResource(ResourceActivationContext context, LocationAugmentedImage resource)
    {
        if (ARWorld.Instance)
        {
            ARWorld.Instance.AddAugmentedMarker(context, resource);
        }
    }

    public override void DeactivateResource(ResourceActivationContext context, LocationAugmentedImage resource)
    {
        if (ARWorld.Instance)
        {
            ARWorld.Instance.RemoveAugmentedMarker(context, resource);
        }
    }
}

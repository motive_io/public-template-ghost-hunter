using Motive.AR.Models;
using Motive.AR.Vuforia;
using Motive.Core.Json;
using Motive.Core.Scripting;
using Motive.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class ARScriptExtensions
{
    public static void Initialize()
    {
        JsonTypeRegistry.Instance.RegisterType("motive.ar.arTask", typeof(ARTask));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.locationAugmentedImage", typeof(LocationAugmentedImage));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.locationAugmented3dAsset", typeof(LocationAugmented3DAsset));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.linearDistanceVariation", typeof(LinearDistanceVariation));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.fixedDistanceVariation", typeof(FixedDistanceVariation));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.locationValuablesCollection", typeof(LocationValuablesCollection));

        JsonTypeRegistry.Instance.RegisterType("motive.ar.visualMarkerMedia", typeof(VisualMarkerMedia));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.visualMarker3dAsset", typeof(VisualMarker3DAsset));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.visualMarkerTrackingCondition", typeof(VisualMarkerTrackingCondition));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.vuforia.frameMarker", typeof(FrameMarker)); // deprecated
        JsonTypeRegistry.Instance.RegisterType("motive.vuforia.vuMark", typeof(VuMark));
        JsonTypeRegistry.Instance.RegisterType("motive.vuforia.imageTarget", typeof(ImageTarget));

        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.ar.arTask", new ARTaskProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.ar.locationAugmentedImage", new LocationAugmentedImageProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.ar.locationAugmented3dAsset", new LocationAugmented3DAssetProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.ar.visualMarkerMedia", new VisualMarkerMediaProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.ar.visualMarker3dAsset", new VisualMarker3DAssetProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.ar.locationValuablesCollection", new LocationValuablesCollectionProcessor());

        // Could live somewhere else eventually:
        JsonTypeRegistry.Instance.RegisterType("motive.unity.asset", typeof(UnityAsset));

#if VUFORIA_ANDROID_SETTINGS || VUFORIA_IOS_SETTINGS
        ScriptEngine.Instance.RegisterConditionMonitor(VuforiaWorld.Instance.TrackingConditionMonitor);
#endif
    }
}

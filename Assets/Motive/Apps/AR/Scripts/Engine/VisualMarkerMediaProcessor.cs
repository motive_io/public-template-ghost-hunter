﻿using Motive.AR.Models;
using Motive.Unity.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Motive.Core.Scripting;

#if MOTIVE_VUFORIA
using Motive.AR.Vuforia;
#endif

public class VisualMarkerMediaProcessor : ThreadSafeScriptResourceProcessor<VisualMarkerMedia>
{
    public override void ActivateResource(ResourceActivationContext context, VisualMarkerMedia resource)
    {
#if MOTIVE_VUFORIA
        VuforiaWorld.Instance.AddMarkerMedia(context, resource);
#endif
    }

    public override void DeactivateResource(ResourceActivationContext context, VisualMarkerMedia resource)
    {
#if MOTIVE_VUFORIA
        VuforiaWorld.Instance.RemoveMarkerMedia(context, resource);
#endif
    }
}

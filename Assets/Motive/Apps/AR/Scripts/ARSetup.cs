﻿using UnityEngine;
using System.Collections;
using Motive.AR.Scripting;
using Motive.AR.LocationServices;
using Motive.UI.Framework;
using Motive;
using Motive.Core.Scripting;
using Motive.Unity.Maps;
using Motive.AR.Media;
using Motive.Core.WebServices;
using Motive.Core;

using Logger = Motive.Core.Diagnostics.Logger;

#if MOTIVE_MAPBOX
using Mapbox.Unity;
#endif

public class ARSetup<T> : MonoBehaviour where T : ARSetup<T> 
{
    public AppConfig AppConfig;
    public bool AllowMultipleScriptRunners;
    public bool AutoLaunchScriptRunners;
    public bool RequireLogin;

    static T sInstance = null;

    private Logger m_logger;

    public static T Instance
    {
        get { return sInstance; }
    }

    protected virtual void Awake()
    {
        m_logger = new Logger(this);

        if (sInstance != null)
        {
            m_logger.Error("SingletonComponent.Awake: error " + name + " already initialized");
        }



        sInstance = (T)this;
    }

	// Use this for initialization
	protected virtual void Start () {
        Platform.Instance.Initialize();

        if (AppConfig)
        {
            WebServices.Instance.AppId = AppConfig.AppId;
            WebServices.Instance.ApiKey = AppConfig.ApiKey;
            WebServices.Instance.SetConfig(AppConfig.ConfigName);
        }

        WebServices.Instance.Initialize();

        DebugPlayerLocation.Instance.Initialize();

        ScriptExtensions.Initialize(
            Platform.Instance.LocationManager,
            Platform.Instance.BeaconManager,
            ForegroundPositionService.Instance.Compass);

        ARScriptExtensions.Initialize();

        ForegroundPositionService.Instance.Initialize();

        ScriptRunnerManager.Instance.AllowMultiple = AllowMultipleScriptRunners;

        WebServices.Instance.AddAssetDirectory(CharacterDirectory.Instance);
        WebServices.Instance.AddAssetDirectory(CollectibleDirectory.Instance);
        WebServices.Instance.AddAssetDirectory(ScriptRunnerDirectory.Instance);
        WebServices.Instance.AddAssetDirectory(RecipeDirectory.Instance);

        AppManager.Instance.OnLoadComplete(() =>
            {
                var objects = ScriptObjectDirectory.Instance.GetAllObjects<Script>();
                ScriptManager.Instance.SetScripts(objects);
            });

        if (AutoLaunchScriptRunners)
        {
            ScriptManager.Instance.ScriptsUpdated += (caller, args) =>
                {
                    ScriptRunnerManager.Instance.LaunchRunningScripts(ScriptRunnerDirectory.Instance);
                };
        }

        ConfigureLocationServices();

        if (DynamicConfig.Instance != null && DynamicConfig.Instance.UseDynamicConfigLogin)
        {
            DynamicConfig.Instance.SpaceSelected += (sender, args) =>
            {
                if (!AppManager.Instance.IsInitialized)
                {
                    AppManager.Instance.Start();
                }
                else
                {
                    AppManager.Instance.Reset();
                }
            };

            DynamicConfig.Instance.Initialize();
        }
        else
        {
            AppManager.Instance.Start();
        }

        ForegroundPositionService.Instance.LocationTracker.GetReading((reading) =>
        {
            OnFirstLocationReading(reading);
        });

        WorldValuablesManager.Instance.Initialize();
            
        LocativeAudioDriver.Instance.Start();

        Platform.Instance.StartSensors();
    }

    protected virtual void OnFirstLocationReading(LocationReading reading)
    {
    }

    protected virtual void ConfigureLocationServices()
    {
#if MOTIVE_MAPBOX
        if (!string.IsNullOrEmpty(MapboxAccess.Instance.Configuration.AccessToken))
        {
            MapboxLocationCacheDriver.Instance.Initialize(MapboxAccess.Instance.Configuration.AccessToken);

            var source = GameObject.FindObjectOfType<MapBoxSource>();

            if (source)
            {
                source.AccessToken = MapboxAccess.Instance.Configuration.AccessToken;
            }
        }
#endif
    }
}

﻿using UnityEngine;
using System.Collections;

public enum BuildConfig
{
    Debug,
    Release
}

public class BuildSettings : SingletonComponent<BuildSettings> {
    public BuildConfig BuildConfig;

    public static bool IsDebug
    {
        get
        {
            return BuildSettings.Instance ?
                BuildSettings.Instance.BuildConfig == BuildConfig.Debug :
                false;
        }
    }

    public static bool IsRelease
    {
        get
        {
            return BuildSettings.Instance ?
                BuildSettings.Instance.BuildConfig == BuildConfig.Release :
                false;
        }
    }
}

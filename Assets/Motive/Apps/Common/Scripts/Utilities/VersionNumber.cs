﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VersionNumber : MonoBehaviour {

    public Text VersionText;

    void Awake()
    {
        if (!VersionText)
        {
            VersionText = GetComponent<Text>();
        }
    }

    void Start()
    {
        VersionText.text = "v " + Application.version;
    }
}

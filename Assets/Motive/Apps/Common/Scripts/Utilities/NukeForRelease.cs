﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NukeForRelease : MonoBehaviour 
{
    void Start()
    {
        if (BuildSettings.Instance.BuildConfig == BuildConfig.Release)
        {
            Destroy(gameObject);
        }
    }
}

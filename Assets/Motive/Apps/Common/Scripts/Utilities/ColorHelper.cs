﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorHelper
{
	public static Color ToUnityColor(Motive.Core.Models.Color color)
	{
		return new Color(color.R, color.G, color.B, color.A);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DisableOnPlatform : MonoBehaviour {

    public RuntimePlatform[] Platforms;

    void Awake()
    {
        if (Platforms != null && Platforms.Contains(Application.platform))
        {
            gameObject.SetActive(false);
        }
    }
}

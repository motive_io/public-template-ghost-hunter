﻿using Motive.Unity.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public static class AnalyticsHelper
{
    // For now just uses Unity Analytics
    public static void FireEvent(string eventName, Dictionary<string, object> args = null)
    {
        ThreadHelper.Instance.CallOnMainThread(() =>
            {
                Analytics.CustomEvent(eventName, args);
            });
    }
}

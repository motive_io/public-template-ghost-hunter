﻿using Motive;
using Motive._3D.Models;
using Motive.Core.Utilities;
using Motive.Unity;
using Motive.Unity.UI;
using Motive.Unity.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class UnityAssetLoader
{
    static Dictionary<string, UnityEngine.AssetBundle> m_loadedBundles =
        new Dictionary<string, UnityEngine.AssetBundle>();

    static SetDictionary<string, Action<UnityEngine.AssetBundle>> m_loadOperations =
        new SetDictionary<string, Action<UnityEngine.AssetBundle>>();
    
    public static void InstantiateAssetInstance<T>(AssetInstance assetInstance, Action<T> onLoad, bool applyLayout = true)
        where T : UnityEngine.Object
    {
        ThreadHelper.Instance.CallOnMainThread(() =>
            {
                var unityAsset = assetInstance.Asset as UnityAsset;

                if (unityAsset != null)
                {
                    ThreadHelper.Instance.StartCoroutine(LoadAsset<T>(unityAsset, (T assetObj) =>
                    {
                    }));
                }
            });
    }

    public static void InstantiateAssetInstance(AssetInstance assetInstance, Action<GameObject> onLoad, bool applyLayout = true)
    {
        InstantiateAssetInstance<GameObject>(assetInstance, onLoad, applyLayout);
    }

    public static IEnumerator LoadAsset<T>(UnityAsset asset, Action<T> onLoad)
        where T : UnityEngine.Object
    {
        if (asset == null)
        {
            onLoad(null);

            yield break;
        }

        if (asset.AssetBundle == null || asset.AssetBundle.Url == null)
        {
            if (asset.AssetName != null)
            {
                var obj = Resources.Load(asset.AssetName) as T;

                onLoad(obj);
            } 
            
            yield break;
        }

        Action<UnityEngine.AssetBundle> onReady = (bundle) => {
            if (bundle != null)
            {
                onLoad(bundle.LoadAsset<T>(asset.AssetName));
            }
            else
            {
                onLoad(null);
            }
        };

        UnityEngine.AssetBundle unityBundle = null;

        if (!m_loadedBundles.TryGetValue(asset.AssetBundle.Url, out unityBundle))
        {
            if (m_loadOperations.ContainsKey(asset.AssetBundle.Url))
            {
                m_loadOperations.Add(asset.AssetBundle.Url, onReady);
            }
            else
            {
                m_loadOperations.Add(asset.AssetBundle.Url, onReady);

                var local = WebServices.Instance.MediaDownloadManager.GetPathForItem(asset.AssetBundle.Url);
                var localUri = (new Uri(local)).AbsoluteUri;

                var www = new WWW(localUri);

                yield return www;

                unityBundle = www.assetBundle;

                m_loadedBundles[asset.AssetBundle.Url] = unityBundle;

                foreach (var call in m_loadOperations[asset.AssetBundle.Url])
                {
                    call(unityBundle);
                }

                m_loadOperations.RemoveAll(asset.AssetBundle.Url);
            }
        }
        else
        {
            onReady(unityBundle);
        }
    }

    internal static void LoadAsset(Motive._3D.Models.AssetInstance assetInstance, GameObject parent)
    {
        var unityAsset = assetInstance.Asset as UnityAsset;

        if (unityAsset == null)
        {
            return;
        }

        ThreadHelper.Instance.StartCoroutine(LoadAsset<GameObject>(unityAsset, (prefab) =>
            {
                if (prefab != null)
                {
                    var obj = GameObject.Instantiate(prefab);

                    obj.transform.SetParent(parent.transform);

                    obj.transform.localPosition = Vector3.zero;
                    obj.transform.localRotation = Quaternion.identity;

                    if (assetInstance.Layout != null)
                    {
                        LayoutHelper.Apply(obj.transform, assetInstance.Layout);
                    }
                }
            }));
    }
}

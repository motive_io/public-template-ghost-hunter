﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;

public static class TextHelper
{
    public static bool CheckOverflow(Text textBox)
    {
        var prefHeight = LayoutUtility.GetPreferredHeight(textBox.rectTransform);
        var prefWidth = LayoutUtility.GetPreferredWidth(textBox.rectTransform);

        return prefHeight > textBox.rectTransform.rect.height ||
                prefWidth > textBox.rectTransform.rect.width;
    }
}

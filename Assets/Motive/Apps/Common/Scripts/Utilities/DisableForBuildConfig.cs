﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisableForBuildConfig : MonoBehaviour
{
    public BuildConfig Config;
    public GameObject[] Objects;
    public MonoBehaviour[] Components;

    void Awake()
    {
        if (BuildSettings.Instance.BuildConfig == Config)
        {
            if (Objects != null)
            {
                foreach (var obj in Objects)
                {
                    if (obj)
                    {
                        obj.SetActive(false);
                    }
                }
            }

            if (Components != null)
            {
                foreach (var com in Components)
                {
                    if (com)
                    {
                        com.enabled = false;
                    }
                }
            }
        }
    }

	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

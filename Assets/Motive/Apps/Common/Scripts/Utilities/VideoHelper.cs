﻿using Motive;
using Motive.Core.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class VideoHelper
{
    public static void PlayFullScreen(MediaItem mediaItem, Action onClose = null)
    {
        if (mediaItem != null)
        {
            Screen.orientation = ScreenOrientation.AutoRotation;
            var localUrl = WebServices.Instance.MediaDownloadManager.GetPathForItem(mediaItem.Url);
            Handheld.PlayFullScreenMovie(localUrl);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using Motive.UI.Framework;
using UnityEngine.UI;
using System.Text;

public class StatusPanel : Panel {
    public Text Text;

    public override void Populate()
    {
        var scripts = ScriptManager.Instance.GetRunningScripts();

        StringBuilder message = new StringBuilder();

        message.Append("Running scripts:\n\n");

        foreach (var script in scripts)
        {
            message.AppendFormat("{0} ({1})\n", script.Name, script.Id);
        }

        Text.text = message.ToString();
    }
}

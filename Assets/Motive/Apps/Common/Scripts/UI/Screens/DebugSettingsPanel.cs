﻿using UnityEngine;
using System.Collections;
using Motive.UI.Framework;
using Motive.AR.LocationServices;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class DebugSettingsPanel : Panel 
{
    public Toggle UseDevCatalogs;
    public Text AppVersion;

    public PanelLink ResetDialog;

    public void Reset()
    {
        if (ResetDialog)
        {
            var panel = ResetDialog.GetPanel<OptionsDialogPanel>();

            if (panel)
            {
                panel.Show(new string[] { "OK", "Cancel" },
                    (opt) =>
                    {
                        if (opt == "OK")
                        {
                            Back();

                            AppManager.Instance.Reset();
                        }
                    });
            }
        }
        else
        {
            OptionsDialogPanel.Show("Reset all data?", new string[] { "OK", "Cancel" },
                (opt) =>
                {
                    if (opt == "OK")
                    {
                        Back();

                        AppManager.Instance.Reset();
                    }
                });
        }
    }

    public override void DidPush()
    {
        if (UseDevCatalogs)
        {
            UseDevCatalogs.isOn = PlayerPrefs.GetInt("Debug_UseDevCatalogs") != 0;
        }

        if (AppVersion)
        {
            AppVersion.text = "v " + Application.version;
        }

        base.DidPush();
    }

    public void ToggleUseDevCatalogs(bool value)
    {
        PlayerPrefs.SetInt("Debug_UseDevCatalogs", value ? 1 : 0);
        PlayerPrefs.Save();
    }

    public void Reload()
    {
        Back();

        AppManager.Instance.Reload();
    }
}

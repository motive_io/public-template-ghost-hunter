﻿using Motive.Gaming.Models;
using Motive.UI.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskExchangeConfirmPanel : Panel<IPlayerTaskDriver> {
    public TaskItem TaskItem;

    bool m_confirm;

    public override void Populate(IPlayerTaskDriver data)
    {
        m_confirm = false;

        TaskItem.Populate(data);

        base.Populate(data);
    }

    public void Confirm()
    {
        m_confirm = true;

        Back();
    }

	public override void DidHide ()
	{
		TaskItem.Reset();

		base.DidHide ();
	}

    public void Show(IPlayerTaskDriver driver, Action onConfirm)
    {
        PanelManager.Instance.Push(this, driver, () =>
            {
                if (m_confirm)
                {
                    if (onConfirm != null)
                    {
                        onConfirm();
                    }
                }
            });
    }
}

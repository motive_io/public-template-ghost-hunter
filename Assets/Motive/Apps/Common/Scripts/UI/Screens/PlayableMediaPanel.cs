﻿using Motive.Core.Models;
using Motive.UI.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayableMediaPanel : Panel<ResourcePanelData<MediaContent>>
{
    public override void Populate(ResourcePanelData<MediaContent> data)
    {
        PopulateComponent<MediaContentComponent>(data.Resource);

        base.Populate(data);
    }
}

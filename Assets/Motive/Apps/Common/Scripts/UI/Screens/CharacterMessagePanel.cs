﻿using System;
using Motive.Core.Media;
using Motive.Gaming.Models;
using Motive.UI.Framework;
using UnityEngine;
using UnityEngine.UI;
using Motive.Unity.Media;

public class CharacterMessagePanel : Panel<ResourcePanelData<CharacterMessage>>
{
    public override void Populate(ResourcePanelData<CharacterMessage> data)
    {
        PopulateComponent<CharacterMessageComponent>(data.Resource);
    }
}




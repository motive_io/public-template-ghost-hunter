﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Motive.Core.Models;
using Motive.Core.Media;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Video;
using UnityEngine.Events;
using Motive.UI.Framework;

public abstract class VideoSubpanel : MonoBehaviour 
{
    public UnityEvent PlaybackCompleted;
    public UnityEvent ClipLoaded;

    public GameObject Embedded;
    public GameObject FullScreen;
    public Text time;
    public bool SwitchModeOnRotate;
    public bool AutoRotateInFullScreen;

    private Action m_onComplete;

    public abstract float AspectRatio { get; }
    public abstract float Position { get; }
    public abstract float Duration { get; }
    public abstract bool IsPlaying { get; }

    bool m_checkedOrientation;
    bool m_isLandscape;

    public virtual void Play(Action onComplete)
    {
        SetOnCompleteHandler(onComplete);

        Play();
    }

    public abstract void Play();

    public abstract void Play(string url);

    public virtual void Play(string url, Action onComplete)
    {
        SetOnCompleteHandler(onComplete);

        Play(url);
    }

    public abstract void Stop();

    public abstract void Pause();

    protected virtual void SetOnCompleteHandler(Action onComplete)
    {
        m_onComplete = onComplete;
    }

    protected virtual void OnComplete()
    {
        if (m_onComplete != null)
        {
            m_onComplete();
        }

        if (PlaybackCompleted != null)
        {
            PlaybackCompleted.Invoke();
        }
    }

    public virtual void Play(MediaItem media, Action onComplete = null)
    {
        if (media != null)
        {
            AudioContentPlayer.Instance.Pause();

            Play(media.Url, onComplete);
        }
    }
    
    public void SetFullScreen(bool fullScreen)
    {
        if (FullScreen && Embedded)
        {
            FullScreen.SetActive(fullScreen);
            Embedded.SetActive(!fullScreen);
        }

        if (AutoRotateInFullScreen)
        {
            if (fullScreen)
            {
                PanelManager.Instance.SetOrientation(ScreenOrientation.AutoRotation);
            }
            else
            {
                PanelManager.Instance.SetOrientation(PanelManager.Instance.DefaultOrientation);
            }
        }
    }

    public void ToggleFullScreen()
    {
        if (FullScreen && Embedded)
        {
            bool fullScreen = FullScreen.activeSelf;

            SetFullScreen(!fullScreen);
        }
    }

    public void PlayPause()
    {
        if (IsPlaying)
        {
            Pause();
        }
        else
        {
            Play();
        }
    }

    protected virtual void Update()
    {
        if (SwitchModeOnRotate)
        {
            if (!m_checkedOrientation)
            {
                m_isLandscape = (Input.deviceOrientation == DeviceOrientation.LandscapeLeft ||
                                 Input.deviceOrientation == DeviceOrientation.LandscapeRight);
            }

            bool isLandscape = (Input.deviceOrientation == DeviceOrientation.LandscapeLeft ||
                                Input.deviceOrientation == DeviceOrientation.LandscapeRight);

            if (isLandscape && (!m_checkedOrientation || isLandscape != m_isLandscape))
            {
                SetFullScreen(true);
            }

            m_isLandscape = isLandscape;
            m_checkedOrientation = true;
        }

        if (time)
        {
            var durationTime = string.Format("{0}:{1:00}", (int) Duration / 60, (int) Duration % 60);
            var prettyTime = string.Format("{0}:{1:00} / {2}", (int) Position / 60, (int) Position % 60, durationTime);
            time.text = prettyTime;

        }
    }

    public abstract void UpdatePosition(float pos);

    internal virtual void Close()
    {
        Stop();

        AudioContentPlayer.Instance.Resume();
    }
}
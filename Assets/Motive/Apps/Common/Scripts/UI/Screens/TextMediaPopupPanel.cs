﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Motive.Unity.Media;
using System;
using Motive.Core.Models;
using Motive.Core.Scripting;
using Motive.Core.Media;
using Motive.UI.Framework;
using Motive;

public class TextMediaPopupPanel : Panel<ITextMediaContent> {
	public Text Text;

    private IAudioPlayer m_player;

    public override void Populate(ITextMediaContent data)
	{
        StopPlayer();

		Text.text = data.Text;

		base.Populate (data);

        if (data.MediaItem != null && data.MediaItem.MediaType == Motive.Core.Media.MediaType.Audio)
        {
            var localUrl = WebServices.Instance.MediaDownloadManager.GetPathForItem(data.MediaItem.Url);

            m_player = UnityAudioPlayerChannel.Instance.CreatePlayer(new Uri(localUrl));

            m_player.Play();

            AudioContentPlayer.Instance.Pause();
        }
	}

    void StopPlayer()
    {
        if (m_player != null)
        {
            m_player.Stop();
            m_player.Dispose();

            m_player = null;
        }
    }

    public override void DidPop()
    {
        StopPlayer();

        AudioContentPlayer.Instance.Resume();

        base.DidPop();
    }
}

﻿using Motive.Gaming.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryScreenMessagePanel : InventoryCollectiblePanel
{
    public override void PopulateContent(Collectible collectible)
    {
        PopulateComponent<ScreenMessageComponent>(collectible.Content);
    }
}

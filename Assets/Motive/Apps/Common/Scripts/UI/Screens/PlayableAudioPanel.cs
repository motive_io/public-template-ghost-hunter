using UnityEngine;
using System.Collections;
using Motive.Core.Models;
using Motive.UI.Framework;
using System;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayableAudioPanel : PlayableMediaPanel
{
    public UnityEvent PlaybackCompleted;

    protected override void Awake()
    {
        var component = GetComponent<AudioContentPlayerComponent>();

        if (component)
        {
            component.PlaybackCompleted.AddListener(() =>
                {
                    if (PlaybackCompleted != null)
                    {
                        PlaybackCompleted.Invoke();
                    }
                });
        }

        base.Awake();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using Motive.UI.Framework;
using Logger = Motive.Core.Diagnostics.Logger;
using Motive.Core.Models;
using UnityEngine.Events;
using UnityEngine;

public class PlayableVideoPanel : PlayableMediaPanel
{
    public UnityEvent PlaybackCompleted;

    protected override void Awake()
    {
        var component = GetComponent<VideoContentPlayerComponent>();

        if (component)
        {
            component.PlaybackCompleted.AddListener(() =>
            {
                if (PlaybackCompleted != null)
                {
                    PlaybackCompleted.Invoke();
                }
            });
        }

        base.Awake();
    }
}

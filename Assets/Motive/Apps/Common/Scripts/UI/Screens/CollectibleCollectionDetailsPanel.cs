using System;
using Motive.UI.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Motive.Core.Models;
using Motive.Gaming.Models;
using UnityEngine;
using UnityEngine.UI;

public class CollectibleCollectionDetailsPanel : Panel<IEnumerable<Collectible>>
{
    public InventoryAudioPanel AudioPanel;
    public InventoryImagePanel ImagePanel;
    public InventoryVideoPanel VideoPanel;

    private List<Collectible> m_collectiblesToShow;
    public override void Populate([NotNull] IEnumerable<Collectible> data)
    {
        base.Populate(data);

        m_collectiblesToShow = null;
        m_collectiblesToShow = Data.ToList();
        PushNextCollectibleOrClose();
    }

    public void PushCollectibleDetails(Collectible collectible, Action onClose = null)
    {
        var content = collectible.Content;

        var mediaContent = content as MediaContent;

        if (mediaContent != null && mediaContent.MediaItem != null)
        {
            switch (mediaContent.MediaItem.MediaType)
            {
                case Motive.Core.Media.MediaType.Video:
                    PanelManager.Instance.Push(VideoPanel, collectible, onClose);
                    return;
                case Motive.Core.Media.MediaType.Audio:
                    PanelManager.Instance.Push(AudioPanel, collectible, onClose);
                    return;
                case Motive.Core.Media.MediaType.Image:
                    PanelManager.Instance.Push(ImagePanel, collectible, onClose);
                    return;
            }
        }

        // Otherwise
        PanelManager.Instance.Push(ImagePanel, collectible, onClose);
    }

    public void PushNextCollectibleOrClose()
    {
        if (m_collectiblesToShow == null || !m_collectiblesToShow.Any())
        {
            Close();
            return;
        }

        var c = m_collectiblesToShow[0];
        m_collectiblesToShow.RemoveAt(0);
        
        if (c == null) { Close();}
        
        PushCollectibleDetails(c, PushNextCollectibleOrClose);

    }
}



﻿using Motive.UI.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskDetailsPanel : Panel<IPlayerTaskDriver> {

    public RawImage Image;
    public Text Title;

    public override void Populate(IPlayerTaskDriver data)
    {
        if (Image && data.Task.ImageUrl != null)
        {
            ImageLoader.LoadImageOnThread(data.Task.ImageUrl, Image);
        }

        if (Title)
        {
            Title.text = data.Task.Title;
        }

        base.Populate(data);
    }
}

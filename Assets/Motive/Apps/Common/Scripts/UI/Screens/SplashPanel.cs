﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Motive.UI.Framework;

public class SplashPanel : Panel {

    public GameObject DownloadPane;
    public Scrollbar DownloadProgress;
    public Text DownloadText;

    private bool m_didPop;
    private bool m_mediaReady;

	public Animator Animator;

	// Use this for initialization
	protected override void Awake () {
        DownloadPane.SetActive(false);
        DownloadProgress.size = 0;
        //GetComponentInChildren<Image>().cro
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void MediaReady()
    {
		Animator.SetBool("MediaReady", true);
    }

    public override void Back()
    {
		base.Back();
    }

    public void SetDownloadStatus(long bytesRead, long totalBytes)
    {
        DownloadPane.SetActive(true);

        DownloadText.text = string.Format("{0} / {1}", (double)bytesRead / 1000, (double)totalBytes / 1000);
        DownloadProgress.size = (float)((double)bytesRead / totalBytes);
    }

    internal void SetStatus(string p)
    {
        //throw new System.NotImplementedException();
    }
}

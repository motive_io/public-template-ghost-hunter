using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Motive.UI.Framework;
using Motive.Gaming.Models;
using UnityEngine.Events;

public class InventoryCraftPanel : InventoryPanel
{
    public bool NeedActivatedRecipeToCraft = false;
    public UnityEvent OnSelectPlayerNeedsRecipe;
    public UnityEvent OnSelectNotCraftableItem;

    protected override void Awake()
    {
        if (OnSelectNotCraftableItem == null)
        {
            OnSelectNotCraftableItem = new UnityEvent();
        }
        if (OnSelectPlayerNeedsRecipe == null)
        {
            OnSelectPlayerNeedsRecipe = new UnityEvent();
        }

        base.Awake();   
    }

    protected override void SelectItem(InventoryTableItem item)
    {
        IEnumerable<Recipe> recipes;

        if (NeedActivatedRecipeToCraft)
        {
            recipes = ActivatedRecipeManager.Instance.GetRecipesForCollectible(item.InventoryCollectible.Collectible.Id);
        }
        else
        {
            recipes = RecipeDirectory.Instance.GetRecipesForCollectible(item.InventoryCollectible.Collectible.Id);
        }

        // For now only look at one recipe per collectible
        var recipe = (recipes == null) ? null : recipes.FirstOrDefault();

        if (recipe != null)
        {
            PopulateComponent<ExecuteRecipeComponent>(recipe);
        }
        else
        {
            var recipeExists = RecipeDirectory.Instance.HasRecipes(item.InventoryCollectible.Collectible.Id);
            if (recipeExists)
            {
                if (OnSelectPlayerNeedsRecipe != null) OnSelectPlayerNeedsRecipe.Invoke();
            }
            else
            {
                if (OnSelectNotCraftableItem != null)  OnSelectNotCraftableItem.Invoke();
            }
        }
    }
}

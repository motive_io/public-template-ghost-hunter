using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

using Logger = Motive.Core.Diagnostics.Logger;
using Motive.UI.Framework;
using Motive.Core.Media;
using Motive;
using Motive.AR.LocationServices;

public class LoadingPanel : Panel
{
    public GameObject ResolvingPane;
    public GameObject DownloadPane;
    public GameObject ReadyPane;
    public GameObject RetryPane;
    public GameObject StartDownloadPane;
    public GameObject WaitingLocationServicesPane;
    public GameObject LocationServicesRequiredPane;

    public Text RetryText;
    public Text StartDownloadDescription;
    public Text StartDownloadButtonText;

    public Scrollbar DownloadProgress;
    public Text DownloadText;

    public bool RequireLocationServices = true;
    bool m_checkLocationServices;

    //bool m_mediaReady;
    WebServicesMediaDownloadState m_currState;
    //Logger m_logger;

    MediaDownloadManager m_downloadManager;

    public event EventHandler OnDownloadError;

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();

        //m_logger = new Logger(this);
        //RetryPane.SetActive(false);
        DownloadPane.SetActive(false);
        DownloadProgress.size = 0;
    }

    public override void DidPush()
    {
        m_checkLocationServices = false;
        m_downloadManager = WebServices.Instance.MediaDownloadManager;

        SetState(WebServices.Instance.DownloadState);

        base.DidPush();
    }

    public void MediaReady()
    {
        m_checkLocationServices = true;
    }

    public void StartDownload()
    {
        StartDownloadPane.gameObject.SetActive(false);

        WebServices.Instance.StartDownload();
    }

    void SetState(WebServicesMediaDownloadState state)
    {
        m_currState = state;

        DownloadPane.gameObject.SetActive(false);

        ObjectHelper.SetObjectActive(RetryPane, false);
        ObjectHelper.SetObjectActive(StartDownloadPane, false);
        ObjectHelper.SetObjectActive(ResolvingPane, false);
        ObjectHelper.SetObjectActive(ReadyPane, false);

        ObjectHelper.SetObjectActive(LocationServicesRequiredPane, false);
        ObjectHelper.SetObjectActive(WaitingLocationServicesPane, false);

        switch (state)
        {
            case WebServicesMediaDownloadState.Resolving:
                ObjectHelper.SetObjectActive(ResolvingPane, true);
                break;
            case WebServicesMediaDownloadState.Error:
                ObjectHelper.SetObjectActive(RetryPane, true);
                break;
            case WebServicesMediaDownloadState.Downloading:
                ObjectHelper.SetObjectActive(DownloadPane, true);
                break;
            case WebServicesMediaDownloadState.Ready:
                ObjectHelper.SetObjectActive(ReadyPane, true);
                break;
            case WebServicesMediaDownloadState.WaitWifi:
                var mb = m_downloadManager.TotalBytes / 1000000;
                StartDownloadDescription.text = string.Format("{0}MB to download. Click below to start download.", mb);
                StartDownloadButtonText.text = string.Format("DOWNLOAD {0}MB", mb);

                ObjectHelper.SetObjectActive(StartDownloadPane, true);
                break;
        }
    }

    void CheckLocationServices()
    {
        if (RequireLocationServices)
        {
            if (Application.isMobilePlatform && !Input.location.isEnabledByUser)
            {
                ObjectHelper.SetObjectActive(WaitingLocationServicesPane, false);
                ObjectHelper.SetObjectActive(LocationServicesRequiredPane, true);
            }
            else if (ForegroundPositionService.Instance.HasLocationData)
            {
                m_checkLocationServices = false;
                Back();
            }
            else
            {
                ObjectHelper.SetObjectActive(LocationServicesRequiredPane, false);
                ObjectHelper.SetObjectActive(WaitingLocationServicesPane, true);
            }
        }
        else
        {
            m_checkLocationServices = false;

            Back();
        }
    }

    void Update()
    {
        if (m_currState != WebServices.Instance.DownloadState)
        {
            SetState(WebServices.Instance.DownloadState);
        }

        if (m_currState == WebServicesMediaDownloadState.Downloading)
        {
            SetDownloadStatus(m_downloadManager.TotalBytesRead, m_downloadManager.TotalBytes);
        }

        if (m_currState == WebServicesMediaDownloadState.Ready &&
            m_checkLocationServices)
        {
            CheckLocationServices();
        }
    }

    public void SetDownloadStatus(long bytesRead, long totalBytes)
    {
        if (DownloadText)
        {
            if (totalBytes == 0)
            {
                DownloadText.text = "";
            }
            else if (totalBytes >= 100000000)
            {
                DownloadText.text = string.Format("{0:0}MB / {1:0}MB", (double)bytesRead / 1000000, (double)totalBytes / 1000000);
            }
            else if (totalBytes >= 1000000)
            {
                DownloadText.text = string.Format("{0:0.0}MB / {1:0.0}MB", (double)bytesRead / 1000000, (double)totalBytes / 1000000);
            }
            else
            {
                DownloadText.text = string.Format("{0:0.0}k / {1:0.0}k", (double)bytesRead / 1000, (double)totalBytes / 1000);
            }
        }

        if (totalBytes == 0)
        {
            DownloadProgress.size = 1;
        }
        else
        {
            DownloadProgress.size = (float)((double)bytesRead / totalBytes);
        }
    }

    public void Retry()
    {
        RetryPane.gameObject.SetActive(false);

        if (DynamicConfig.UseDynamicConfig)
        {
            Back();

            DynamicConfig.Instance.Retry();
        }
        else
        {
            AppManager.Instance.Reload();
        }
    }

    internal void SetStatus(string p)
    {
        DownloadText.gameObject.SetActive(true);
        DownloadText.text = p;
    }

    public void DownloadError(string p)
    {
        SetStatus(p);
        if (OnDownloadError != null) OnDownloadError(this, EventArgs.Empty);
        OnDownloadError = null;
    }
}

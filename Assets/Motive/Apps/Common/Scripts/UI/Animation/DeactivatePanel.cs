﻿using Motive.UI.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class DeactivatePanel : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var panel = animator.gameObject.GetComponentInParent<Panel>();
        panel.SetActive(false);
        panel.gameObject.transform.localScale = Vector3.one;
    }
}

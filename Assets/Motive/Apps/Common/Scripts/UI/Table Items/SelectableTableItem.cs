﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SelectableTableItem : MonoBehaviour, IPointerClickHandler
{
    public UnityEvent OnSelected;
    public bool Selectable = true;

    public GameObject EnabledWhenSelected;
    public GameObject EnabledWhenNotSelected;

    public virtual void Select()
    {
        if (OnSelected != null)
        {
            OnSelected.Invoke();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (Selectable)
        {
            Select();
        }
    }
}

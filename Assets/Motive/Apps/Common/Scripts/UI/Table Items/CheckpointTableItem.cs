﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckpointTableItem : SelectableTableItem
{
    public Text Title;
    public Text Time;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskNotification : TextImageItem 
{
	public IPlayerTaskDriver TaskDriver;

	public virtual void Populate(IPlayerTaskDriver driver)
	{
		TaskDriver = driver;

		if (driver != null)
		{
            SetText(driver.Task.Title);
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

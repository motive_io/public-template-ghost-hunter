using Motive.Gaming.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleTableItem : TextImageItem
{
    public Collectible Collectible { get; private set; }

    public virtual void Populate(Collectible collectible)
    {
        this.Collectible = collectible;

        SetText(collectible.Title);
        SetImage(collectible.ImageUrl);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttractionItemBase : TextImageItem
{
    public Text Description;
    public GameObject Locked;
    public GameObject Unlocked;
}
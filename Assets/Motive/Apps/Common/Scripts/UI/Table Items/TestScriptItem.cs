using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Motive.Core.Scripting;
using Motive.UI.Framework;
using System;

public class TestScriptItemEventArgs : EventArgs
{
	public Script Script { get; private set; }

	public TestScriptItemEventArgs(Script _script)
	{
		Script = _script;
	}
}

public class TestScriptItem : MonoBehaviour {
	public Button LaunchButton;
	public Button StopButton;

    public Text ScriptName;
    public Text UpdatedOn;

	public event EventHandler<TestScriptItemEventArgs> Launch;
	public event EventHandler<TestScriptItemEventArgs> Stop;
	public event EventHandler<TestScriptItemEventArgs> Reset;

    Script m_script;

    public void Populate(Script script)
    {
        m_script = script;

        ScriptName.text = script.Name;
        UpdatedOn.text = script.LastModifiedTime.ToString();

		UpdateState();
    }

	public void UpdateState()
	{
		var isRunning = ScriptManager.Instance.IsScriptRunning(m_script, "test");

		LaunchButton.gameObject.SetActive(!isRunning);
		StopButton.gameObject.SetActive(isRunning);
	}

    public void DoLaunch()
    {
		if (Launch != null)
		{
			Launch(this, new TestScriptItemEventArgs(m_script));
		}
    }

	public void DoStop()
	{
		if (Stop != null)
		{
			Stop(this, new TestScriptItemEventArgs(m_script));
		}
	}

    public void DoReset()
	{
		if (Reset != null)
		{
			Reset(this, new TestScriptItemEventArgs(m_script));
		}
    }
}

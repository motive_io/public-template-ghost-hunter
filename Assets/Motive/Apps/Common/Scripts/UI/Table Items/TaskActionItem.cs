using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TaskActionItem : MonoBehaviour {
    public RawImage Image;
    public Text ItemName;
    public Text Count;
    public GameObject SatisfiedObject;
    public string Id { get; set; }
}

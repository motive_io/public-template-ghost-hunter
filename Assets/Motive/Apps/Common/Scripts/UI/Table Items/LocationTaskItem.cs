﻿using UnityEngine;
using System.Collections;
using Motive.AR.Models;
using Motive.Unity.Maps;
using Motive.UI.Framework;
using System.Linq;

public class LocationTaskItem : TaskItem 
{
    public override void Populate(IPlayerTaskDriver driver)
    {
        var locDriver = driver as LocationTaskDriver;

        if (Subtitle)
        {
            Subtitle.text = "";

            if (locDriver.Task.Locations != null)
            {
                var loc = locDriver.Task.Locations.FirstOrDefault();

                if (loc != null && Subtitle)
                {
                    Subtitle.text = loc.Name;
                }
            }
        }

        base.Populate(driver);
    }

    public void ShowLocation()
    {
        LocationTaskAnnotationHandler.Instance.SelectTaskAnnotation(Driver);
        PanelManager.Instance.HideAll();
    }
}

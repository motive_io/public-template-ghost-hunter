﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Motive.Core.Scripting;
using Motive.Gaming.Models;

public class TextMediaResponseItem : TextImageItem
{
    public Button Button;

    public virtual void Populate(TextMediaResponse response)
    {
        SetText(response.Text);
        SetImage(response.ImageUrl);
    }
}

﻿using Motive.UI.Framework;
using Motive.Unity.AR;
using Motive.Unity.Maps;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class TaskPreviewPanelHandler : SelectedLocationPanelHandler
{
    Panel m_toDoPanel;
    object m_toDoPanelData;
    bool m_needsUpdate;
    IPlayerTaskDriver m_currTodoDriver;

    // Other panels not linked to annotation handlers
    public Panel CharacterTaskPanel;
    public Panel SimpleTaskPanel;

    protected override void Awake()
    {
        base.Awake();

        AppManager.Instance.Initialized += App_Initialized;
    }

    void App_Initialized(object sender, System.EventArgs e)
    {
        AttractionManager.Instance.OnUpdated.AddListener(HandleUpdated);
        TaskManager.Instance.Updated += TasksUpdated;
        ARWorld.Instance.OnUpdated.AddListener(HandleUpdated);
    }

    private void HandleUpdated()
    {
        m_needsUpdate = true;
    }

    void TasksUpdated(object sender, System.EventArgs e)
    {
        m_needsUpdate = true;
    }

    public void SetToDoPanel(Panel panel, object data = null)
    {
        // Don't re-push the same panel with the same data
        if (panel == CurrentPanel && 
            panel != null &&
            panel.Data == data)
        {
            return;
        }

        var oldToDoPanel = m_toDoPanel;
        m_toDoPanelData = data;
        m_toDoPanel = panel;

        if (oldToDoPanel != null && oldToDoPanel != m_toDoPanel)
        {
            oldToDoPanel.Back();
        }

        ShowSelectedLocationPanel(m_toDoPanel, data);

        /*
        if (CurrentPanel == null && m_toDoPanel != null)
        {
            if (oldToDoPanel && oldToDoPanel != m_toDoPanel)
            {
                oldToDoPanel.Back();
            }

            ShowSelectedLocationPanel(m_toDoPanel, data);

            //PanelManager.Instance.Push(m_toDoPanel, data);
        }
        else if (oldToDoPanel != null && m_toDoPanel == null)
        {
            oldToDoPanel.Back();
        }*/
    }

    public override void ShowSelectedLocationPanel(Panel toShow, object data, Action onClose = null)
    {
        base.ShowSelectedLocationPanel(toShow, data, () =>
            {
                m_needsUpdate = true;
                if (CurrentPanel == toShow)
                {
                    CurrentPanel = null;
                }

                if (onClose != null)
                {
                    onClose();
                }
            });
    }

    public override void HideSelectedLocationPanel(Panel toHide, object data)
    {
        if (toHide != null && toHide == m_toDoPanel)
        {
            if (toHide.Data == m_toDoPanelData)
            {
                // Same panel, same data as the todo panel. Leave it open.
                return;
            }
        }
        else
        {
            base.HideSelectedLocationPanel(toHide, data);
        }

        if (m_toDoPanel != null)
        {
            PanelManager.Instance.Push(m_toDoPanel, m_toDoPanelData);
        }
    }
    
    public static new TaskPreviewPanelHandler Instance
    {
        get
        {
            return (TaskPreviewPanelHandler)SelectedLocationPanelHandler.Instance;
        }
    }

    // TODO: everything below here is a bit of a grab-bag that needs a refactor.
    // Essentially, task driver delegates should handle the todo list in some way.
    // For now, we know what we want each one to do so we'll do it directly.
    public void SetTodoTask(IPlayerTaskDriver driver)
    {
        // Don't preview complete/closed tasks
        if (driver != null && (driver.IsComplete || driver.ActivationContext.IsClosed))
        {
            driver.SetFocus(false);

            SetToDoPanel(null);

            return;
        }

        if (m_currTodoDriver != null)
        {
            m_currTodoDriver.SetFocus(false);
        }

        m_currTodoDriver = driver;

        if (driver != null)
        {
            if (driver is LocationTaskDriver)
            {
                var ann = LocationTaskAnnotationHandler.Instance.GetNearestAnnotation((LocationTaskDriver)driver);

                if (ann != null)
                {
                    MapController.Instance.FocusAnnotation(ann);
                }
                else
                {
                    SetToDoPanel(LocationTaskAnnotationHandler.Instance.SelectedLocationPanel, driver);
                }
            }
            else if (driver is ARTaskDriver)
            {
                var ann = ARTaskAnnotationHandler.Instance.GetNearestAnnotation((ARTaskDriver)driver);

                if (ann != null)
                {
                    MapController.Instance.FocusAnnotation(ann);
                }
                else
                {
                    SetToDoPanel(ARTaskAnnotationHandler.Instance.SelectedLocationPanel, driver);
                }
            }
            else if (driver.Task.Type == "motive.gaming.characterTask")
            {
                SetToDoPanel(CharacterTaskPanel, driver);
            }
            else
            {
                SetToDoPanel(SimpleTaskPanel, driver);
            }

            driver.SetFocus(true);
        }
        else
        {
            SetToDoPanel(null);
        }

        /*
        if (m_annotation != null)
        {
            // TODO: this belongs in a driver delegate
            var obj = driver.GetWorldObjectAt(m_annotation.Location);

            if (obj != null)
            {
                ARViewManager.Instance.SetGuide(new ARGuideData
                {
                    Instructions = driver.Task.Title,
                    WorldObject = obj,
                    Range = Data.Task.ActionRange
                });
            }
        }*/
    }

    public void ClearTodoTask(IPlayerTaskDriver driver)
    {
        if (driver == m_currTodoDriver)
        {
            SetTodoTask(null);
        }
    }

    void Update()
    {
        if (m_needsUpdate)
        {
            m_needsUpdate = false;

            // Only update the preview panel if:
            // 1. There is no current panel, or
            // 2. The current driver is either inactive, complete, or closed
            // Otherwise we don't change anything because we want this panel to be
            // somewhat sticky.
            if (m_currTodoDriver != null &&
				(!TaskManager.Instance.ActiveTaskDrivers.Contains(m_currTodoDriver) ||
					m_currTodoDriver.IsComplete || 
					m_currTodoDriver.ActivationContext.IsClosed))
            {
                ClearTodoTask(m_currTodoDriver);
            }
            else if (CurrentPanel)
            {
                return;
            }

            IPlayerTaskDriver driver = null;

            if (m_currTodoDriver != null && TaskManager.Instance.ActiveTaskDrivers.Contains(m_currTodoDriver))
            {
                driver = m_currTodoDriver;
            }
            else
            {
                driver = TaskManager.Instance.ActiveTaskDrivers
                    .OrderBy(d => d.ActivationContext.ActivationTime)
                    .FirstOrDefault();
            }

            if (driver != null)
            {
                SetTodoTask(driver);
            }
            else
            {
                var todo = AttractionManager.Instance.GetNextToDo();

                if (todo != null)
                {
                    var annotation = AttractionManager.Instance.GetAnnotation(todo.Attraction.Id);

                    SetToDoPanel(AttractionManager.Instance.SelectedLocationPanel, annotation);
                }
                else
                {
                    SetToDoPanel(null);
                }
            }
        }
    }
}

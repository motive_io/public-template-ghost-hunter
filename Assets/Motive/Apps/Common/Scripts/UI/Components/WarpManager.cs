﻿using Motive.AR.LocationServices;
using Motive.UI.Framework;
using Motive.Unity.Maps;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpManager : MonoBehaviour 
{
    public WarpPanel WarpPanel;
    public WarpPanel UnwarpPanel;
    public GameObject UnwarpButton;
	public string WarpDebugSetting;

    void Start()
    {
        MapController.Instance.OnHold.AddListener(Warp);
    }

    void Warp(MapControllerEventArgs args)
    {
		bool warp = string.IsNullOrEmpty(WarpDebugSetting) ||
			SettingsHelper.IsDebugSet(WarpDebugSetting);
		
		if (warp)
		{
			PanelManager.Instance.Push(WarpPanel, args.Coordinates);
		}
    }

	public void Unwarp()
	{
		ForegroundPositionService.Instance.SetAnchorPosition(null);
	}

    void Update()
    {
        if (UnwarpButton)
        {
            UnwarpButton.SetActive(UserLocationService.Instance.AnchorPosition != null);
        }
    }
}

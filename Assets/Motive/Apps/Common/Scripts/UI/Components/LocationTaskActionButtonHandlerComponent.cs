﻿using Motive.UI.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationTaskActionButtonHandlerComponent : TaskActionButtonHandlerComponent<LocationTaskDriver>
{
    public GameObject[] ARActionObjects;

    public override void Populate(LocationTaskDriver obj)
    {
        if (obj.MinigameDriver != null &&
            obj.MinigameDriver is ARCatcherMinigameDriver)
        {
            ObjectHelper.SetObjectsActive(ARActionObjects, true);

            DisableAllObjects();
        }
        else
        {
            ObjectHelper.SetObjectsActive(ARActionObjects, false);

            base.Populate(obj);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class UserInputSlider : Slider 
{
    public bool IsUserUpdate { get; private set; }
    
    public override void OnPointerDown(UnityEngine.EventSystems.PointerEventData eventData)
    {
        IsUserUpdate = true;

        base.OnPointerDown(eventData);
    }

    public override void OnPointerUp(UnityEngine.EventSystems.PointerEventData eventData)
    {
        IsUserUpdate = false;

        base.OnPointerUp(eventData);
    }
}

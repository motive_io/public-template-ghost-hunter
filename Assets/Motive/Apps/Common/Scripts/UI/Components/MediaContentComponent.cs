﻿using Motive.Core.Models;
using Motive.UI.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class MediaContentComponent : PanelComponent<MediaContent>
{
    public Text Title;

    public override void Populate(MediaContent obj)
    {
        if (Title)
        {
            Title.text = obj.Title;
        }

        base.Populate(obj);
    }
}

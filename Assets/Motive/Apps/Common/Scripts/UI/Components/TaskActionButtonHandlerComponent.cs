﻿using Motive.UI.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskActionButtonHandlerComponent : TaskActionButtonHandlerComponent<IPlayerTaskDriver> 
{
}

public class TaskActionButtonHandlerComponent<T> : PanelComponent<T>
    where T : IPlayerTaskDriver
{
    public GameObject[] TakeActionObjects;
	public GameObject[] PutActionObjects;
	public GameObject[] ConfirmActionObjects;

    public override void Populate(T obj)
    {
		DisableAllObjects();

        if (obj.IsGiveTask)
        {
            ObjectHelper.SetObjectsActive(PutActionObjects, true);
        }
		else if (obj.IsTakeTask)
        {
            ObjectHelper.SetObjectsActive(TakeActionObjects, true);
        }
		else if (obj.IsConfirmTask)
		{
			ObjectHelper.SetObjectsActive(ConfirmActionObjects, true);
		}
    }
    
    public override void Populate(object obj)
    {
        base.Populate(obj);
    }

    public void DisableAllObjects()
    {
        ObjectHelper.SetObjectsActive(PutActionObjects, false);
		ObjectHelper.SetObjectsActive(TakeActionObjects, false);
		ObjectHelper.SetObjectsActive(ConfirmActionObjects, false);
    }
}

﻿using Motive.UI.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TodoTaskSetterComponent : PanelComponent<IPlayerTaskDriver>
{
    public override void DidShow(IPlayerTaskDriver obj)
    {
        TaskPreviewPanelHandler.Instance.SetTodoTask(obj);

        base.DidShow(obj);
    }

    public override void DidHide()
    {
        TaskPreviewPanelHandler.Instance.ClearTodoTask(Data);

        base.DidHide();
    }
}

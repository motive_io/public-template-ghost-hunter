﻿using Motive.AR.LocationServices;
using Motive.UI.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WayfinderComponent : PanelComponent<Location> 
{
    public Text Distance;
    public RotateWithCompass Compass;

    public override void Populate(Location obj)
    {
        if (Compass)
        {
            Compass.SetPointAtLocation(obj);
        }

        base.Populate(obj);
    }

    void Update()
    {
        if (Data != null)
        {
            if (Distance)
            {
                var d = ForegroundPositionService.Instance.Position.GetDistanceFrom(Data.Coordinates);

                Distance.text = StringFormatter.FormatDistance(d);
            }
        }
    }
}

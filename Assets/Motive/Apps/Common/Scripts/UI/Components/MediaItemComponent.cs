﻿using Motive.Core.Media;
using Motive.UI.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MediaItemComponent : PanelComponent<MediaItem> 
{
    public abstract override void Populate(MediaItem obj);
}

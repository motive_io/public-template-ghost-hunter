﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleObject : MonoBehaviour 
{
    public GameObject[] ShowWhenOn;
    public GameObject[] ShowWhenOff;

    public bool IsOn;

    void Awake()
    {
        SetIsOn(IsOn);
    }

    public void SetIsOn(bool isOn)
    {
        this.IsOn = isOn;

        ObjectHelper.SetObjectsActive(ShowWhenOn, isOn);
        ObjectHelper.SetObjectsActive(ShowWhenOff, !isOn);
    }

    public void Toggle()
    {
        SetIsOn(!IsOn);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsToggle : MonoBehaviour {

    public Toggle Toggle;
    public string SettingName;

	// Use this for initialization
	void Awake () 
    {
        Toggle.isOn = PlayerPrefs.GetInt(SettingName) == 1;
	}

    public void ToggleValue()
    {
        PlayerPrefs.SetInt(SettingName, Toggle.isOn ? 1 : 0);

        PlayerPrefs.Save();
    }
}

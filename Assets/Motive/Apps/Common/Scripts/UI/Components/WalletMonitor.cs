﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WalletMonitor : MonoBehaviour 
{
    public string Currency;
    public Text Text;

    bool m_updated;

    void Awake()
    {
        AppManager.Instance.OnLoadComplete(() =>
        {
            Wallet.Instance.Updated += Instance_Updated;

            UpdateText();
        });
    }

    private void Instance_Updated(object sender, WalletUpdateEventArgs e)
    {
        m_updated = true;
    }

    void Update()
    {
        if (m_updated)
        {
            UpdateText();
        }
    }

    void UpdateText()
    {
        var val = Wallet.Instance.GetCount(Currency);
        Text.text = val.ToString();
    }
}

﻿using Motive.Gaming.Models;
using Motive.UI.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExecuteRecipeComponent : PanelComponent<Recipe>
{
    public PanelLink ExecuteRecipePanel;

    public override void DidShow()
    {
        if (ExecuteRecipePanel)
        {
            ExecuteRecipePanel.Back();
        }

        base.DidShow();
    }

    public override void Populate(Recipe recipe)
    {
        if (ExecuteRecipePanel)
        {
            ExecuteRecipePanel.Push(recipe);
        }
    }
}

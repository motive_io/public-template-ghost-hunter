﻿using Motive.Core.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageContentComponent : MediaContentComponent
{
    public RawImage Image;

    public override void Populate(MediaContent content)
    {
        if (content.MediaItem != null)
        {
            ImageLoader.LoadImageOnMainThread(content.MediaItem.Url, Image);
        }

        base.Populate(content);
    }
}

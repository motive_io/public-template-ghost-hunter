﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonClickSound : MonoBehaviour {
    Button m_button;
    AudioSource m_audioSource;

    public AudioClip Sound;

    void Awake()
    {
        m_button = gameObject.GetComponent<Button>();
        m_audioSource = gameObject.AddComponent<AudioSource>();

        if (m_button)
        {
            m_button.onClick.AddListener(PlaySound);
        }
    }

    void PlaySound()
    {
        m_audioSource.PlayOneShot(Sound);
    }
}

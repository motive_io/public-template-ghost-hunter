﻿using Motive;
using Motive.AR.LocationServices;
using Motive.Core.Models;
using Motive.Unity.Maps;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationCatalogAnnotationHandler : SimpleMapAnnotationHandler 
{
    public string CatalogName;

    Catalog<Location> m_locationCatalog;

    protected override void Awake()
    {
        base.Awake();

        AppManager.Instance.Started += (caller, args) =>
            {
                RefreshCatalog();
            };
    }

    public void RefreshCatalog()
    {
        WebServices.Instance.LoadCatalog<Location>(CatalogName, (catalog) =>
            {
                m_locationCatalog = catalog;
                
                foreach (var location in m_locationCatalog)
                {
                    var ann = new MapAnnotation(location);
                    AddAnnotation(location.Id, ann);
                }
            });
    }
}

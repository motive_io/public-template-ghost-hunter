using Motive.Core.Utilities;
using Motive.Gaming.Models;
using Motive.UI.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class RewardEventArgs : EventArgs
{
    public ValuablesCollection Valuables { get; private set; }

    public RewardEventArgs(ValuablesCollection valuables)
    {
        this.Valuables = valuables;
    }
}

/// <summary>
/// Handles giving player "rewards."
/// </summary>
public class RewardManager : Singleton<RewardManager>
{
    public Panel RewardPanel { get; set; }
    public event EventHandler<RewardEventArgs> RewardAdded;

    public void ActivatePlayerReward(PlayerReward reward)
    {
        if (reward.Reward != null)
        {
            RewardValuables(reward.Reward);
        }
    }

    public void RewardValuables(ValuablesCollection valuables, Action onReward = null)
    {
        if (RewardAdded != null)
        {
            RewardAdded(this, new RewardEventArgs(valuables));
        }

        if (RewardPanel)
        {
            TransactionManager.Instance.AddValuables(valuables);

            if (onReward != null)
            {
                onReward();
            }

            PanelManager.Instance.Push(RewardPanel, valuables);
        }
        else
        {
            TransactionManager.Instance.AddValuables(valuables);
        }
    }
}

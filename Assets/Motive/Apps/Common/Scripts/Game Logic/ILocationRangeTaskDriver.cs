﻿using Motive.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface ILocationRangeTaskDriver : IPlayerTaskDriver
{
    DoubleRange ActionRange { get; }
}

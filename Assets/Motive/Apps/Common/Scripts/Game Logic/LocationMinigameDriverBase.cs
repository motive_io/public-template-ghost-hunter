﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public abstract class LocationMinigameDriverBase : ILocationMinigameDriver
{
    public LocationTaskDriver TaskDriver { get; private set; }
    public abstract bool ShowMapAnnotation { get; }
    public abstract bool ShowActionButton { get; }

    public virtual string ActionButtonText
    {
        get { return Localize.GetLocalizedString("Task.InRange", "In Range"); }
    }

    public virtual string OutOfRangeActionButtonText
    {
        get { return Localize.GetLocalizedString("Task.OutOfRange", "Out of Range"); }
    }

    protected LocationMinigameDriverBase(LocationTaskDriver taskDriver)
    {
        this.TaskDriver = taskDriver;
    }

    public virtual void Action()
    {
        TaskManager.Instance.Complete(this.TaskDriver);
    }

    public virtual void ShowTask()
    {
    }

    public virtual void HideTask()
    {
    }

    public virtual void SetFocus(bool focus)
    {
    }

    public virtual void Start()
    {
    }

    public virtual void Stop()
    {
    }
}

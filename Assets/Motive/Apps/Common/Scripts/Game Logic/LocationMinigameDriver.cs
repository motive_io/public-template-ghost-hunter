﻿using Motive.Gaming.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public abstract class LocationMinigameDriver<T> : LocationMinigameDriverBase where T : ITaskMinigame
{
    public T Minigame { get; private set; }

    protected LocationMinigameDriver(LocationTaskDriver taskDriver, T minigame)
        : base(taskDriver)
    {
        this.Minigame = minigame;
    }

}

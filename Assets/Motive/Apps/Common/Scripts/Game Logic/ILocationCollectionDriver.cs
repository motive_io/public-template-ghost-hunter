﻿using Motive.AR.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILocationCollectionDriver
{
    void Initialize();

    void StartCollecting();
    void StopCollecting();

    void SpawnItem(LocationSpawnItemDriverEventArgs<Motive.AR.Models.LocationValuablesCollection> e, ILocationCollectionMechanic collectOptions);

    void RemoveItem(LocationSpawnItemDriverEventArgs<Motive.AR.Models.LocationValuablesCollection> e);
}

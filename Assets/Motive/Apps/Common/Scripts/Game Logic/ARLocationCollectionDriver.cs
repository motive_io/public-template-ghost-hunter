﻿using Motive.AR.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Motive.Unity.Utilities;
using Motive.Unity.AR;
using Motive.Gaming.Models;

public class ARLocationCollectionDriver : MonoBehaviour, ILocationCollectionDriver
{
    public MapLocationCollectionDriver MapLocationCollectionDriver;

    Dictionary<string, ARWorldObject> m_worldObjects;

    void Awake()
    {
        WorldValuablesManager.Instance.RegisterCollectionDriver("motive.ar.arLocationCollectionMechanic", this);

		if (!MapLocationCollectionDriver)
		{
			MapLocationCollectionDriver = GetComponent<MapLocationCollectionDriver>();
		}
    }

    public void Initialize()
    {
        m_worldObjects = new Dictionary<string, ARWorldObject>();
    }

    public void StartCollecting()
    {
    }

    public void StopCollecting()
    {
    }

    private ILocationAugmentedOptions GetOptions(
        LocationValuablesCollection lvc, ARLocationCollectionMechanic m)
    {
		var opts = m.AROptions ?? ARWorld.Instance.GetDefaultImageOptions();

		if (lvc.CollectOptions != null && opts.VisibleRange == null)
        {
            opts.VisibleRange = lvc.CollectOptions.CollectRange;
        }

        return opts;
    }

    public void SpawnItem(LocationSpawnItemDriverEventArgs<LocationValuablesCollection> e, ILocationCollectionMechanic m)
    {
        var arMechanic = m as ARLocationCollectionMechanic;
        bool showAnnotation = arMechanic != null && arMechanic.ShowMapAnnotation;

        var collectible = ValuablesCollection.GetFirstCollectible(e.Results.SpawnItem.ValuablesCollection);

        var options = GetOptions(e.Results.SpawnItem, m as ARLocationCollectionMechanic);

        if (collectible != null)
        {
			ThreadHelper.Instance.CallOnMainThread(() =>
				{
					ARWorldObject worldObj = null;

					if (collectible.AssetInstance != null &&
						collectible.AssetInstance.Asset != null)
					{
						var obj = ARWorld.Instance.AddAugmentedAsset(
							e.Results.SourceLocation,
							collectible.AssetInstance,
							options);

						if (obj)
						{
							worldObj = obj.WorldObject;
						}
					}

					if (worldObj == null)
					{
						var obj = ARWorld.Instance.
							AddAugmentedImage(e.Results.SourceLocation, collectible.ImageUrl, options);

						worldObj = obj.WorldObject;
					}

					worldObj.Clicked += (sender, args) =>
					{
						RewardPanel.Show(e.Results.SpawnItem.ValuablesCollection);

						WorldValuablesManager.Instance.Collect(e);
					};

					m_worldObjects[e.Results.SourceLocation.Id] = worldObj;
				});

			if (showAnnotation && MapLocationCollectionDriver)
			{
				MapLocationCollectionDriver.AddARCollectAnnotation(e);
			}
        }
    }

    public void RemoveItem(LocationSpawnItemDriverEventArgs<Motive.AR.Models.LocationValuablesCollection> e)
    {
        if (m_worldObjects.ContainsKey(e.Results.SourceLocation.Id))
        {
            var obj = m_worldObjects[e.Results.SourceLocation.Id];

			ThreadHelper.Instance.CallOnMainThread(() =>
				{
					ARWorld.Instance.RemoveWorldObject(obj);

					if (MapLocationCollectionDriver)
					{
						MapLocationCollectionDriver.RemoveAnnotation(e);
					}
				});
        }
    }
}

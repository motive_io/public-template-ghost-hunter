﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IMinigameDriver
{
    void Action();

    void Start();

    void Stop();

    void SetFocus(bool focus);
}

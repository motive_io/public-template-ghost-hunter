﻿using UnityEngine;
using System.Collections;
using Motive.Core.Scripting;
using Motive.AR.LocationServices;
using Motive.Core.Diagnostics;
using System.Linq;
using Motive.Core.Timing;
using Motive.AR.Models;
using Motive.Unity.Maps;
using Motive.Gaming.Models;
using System;
using Motive.AR.Vuforia;
using Motive.UI;
using Motive.Unity.Utilities;

#if VUFORIA_ANDROID_SETTINGS || VUFORIA_IOS_SETTINGS
public class VisualMarkerTaskDriver : PlayerTaskDriver<VisualMarkerTask>
{
    public bool IsTrackingAnyMarkers { get; private set; }

    public VisualMarkerTaskDriver(ResourceActivationContext context, VisualMarkerTask task)
        : base(context, task)
    {

    }

    public override void Start()
    {
        if (!ActivationContext.IsOpen)
        {
            return;
        }

        ThreadHelper.Instance.CallOnMainThread(ShowTask);

        base.Start();
    }

    public override void Stop()
    {
        ThreadHelper.Instance.CallOnMainThread(HideTask);

        base.Stop();
    }

    protected virtual void HideTask()
    {
        VuforiaWorld.Instance.Activated.RemoveListener(UpdateState);
        VuforiaWorld.Instance.TrackingConditionMonitor.Updated -= TrackingConditionMonitor_Updated;

        if (IsTakeTask)
        {
            VuforiaWorld.Instance.RemoveResourceObjects(ActivationContext.InstanceId);
        }
        else
        {
            ARViewManager.Instance.RemoveInteractiveCollectibles(ActivationContext.InstanceId);
        }
    }

    protected virtual void ShowTask()
    {
        if (IsTakeTask)
        {
            var collectible = FirstCollectible;

            if (collectible != null && Task.VisualMarkers != null)
            {
                Action onSelect = () =>
                {
                    Action();
                };

                // Let the image float on top of the page
                var imageLayout = new Layout()
                {
                    Position = new Motive.Core.Models.Vector(0, 0, 0.2)
                };

                foreach (var m in Task.VisualMarkers)
                {
                    if (collectible.AssetInstance != null)
                    {
                        VuforiaWorld.Instance.Add3DAsset((IVuforiaMarker)m, ActivationContext.InstanceId, collectible.AssetInstance, collectible.ImageUrl, imageLayout, null, onSelect);
                    }
                    else
                    {
                        VuforiaWorld.Instance.AddMarkerImage((IVuforiaMarker)m, ActivationContext.InstanceId, collectible.ImageUrl, imageLayout, null, onSelect);
                    }
                }
            }
        }
        else
        {
            UpdateState();

            VuforiaWorld.Instance.TrackingConditionMonitor.Updated += TrackingConditionMonitor_Updated;
            VuforiaWorld.Instance.Activated.AddListener(UpdateState);
        }
    }

    public bool CheckAllMarkersTracking()
    {
        bool allTracking = true;

        if (Task.VisualMarkers != null)
        {
            foreach (var marker in Task.VisualMarkers)
            {
                allTracking &= VuforiaWorld.Instance.IsTracking((IVuforiaMarker)marker);
            }
        }

        return allTracking;
    }

    public bool CheckAnyMarkersTracking()
    {
        if (Task.VisualMarkers != null)
        {
            foreach (var marker in Task.VisualMarkers)
            {
                if (VuforiaWorld.Instance.IsTracking((IVuforiaMarker)marker))
                {
                    return true;
                }
            }
        }

        return false;
    }

    void CheckAndPlaceItems()
    {
        var wasTracking = IsTrackingAnyMarkers;
        IsTrackingAnyMarkers = CheckAnyMarkersTracking();

        if (!wasTracking && IsTrackingAnyMarkers)
        {
            IsTrackingAnyMarkers = true;

            var collectible = FirstCollectible;

            if (Inventory.Instance.GetCount(collectible.Id) > 0)
            {
                if (collectible != null)
                {
                    ARViewManager.Instance.AddInteractiveCollectible(ActivationContext.InstanceId, collectible, () =>
                    {
                        Action();
                    });
                }
            }
        }
        else if (!IsTrackingAnyMarkers)
        {
            ARViewManager.Instance.RemoveInteractiveCollectibles(ActivationContext.InstanceId);
        }
    }

    void UpdateState()
    {
        if (IsGiveTask)
        {
            CheckAndPlaceItems();
        }
        else
        {
            if (Task.Action == "track")
            {
                if (CheckAllMarkersTracking())
                {
                    Action();
                }
            }
        }
    }

    private void TrackingConditionMonitor_Updated(object sender, EventArgs e)
    {
        UpdateState();
    }
}
#endif
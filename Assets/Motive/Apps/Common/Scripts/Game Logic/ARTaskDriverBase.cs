﻿using Motive.AR.Models;
using Motive.Core.Scripting;
using Motive.Gaming.Models;
using Motive.UI;
using Motive.Unity.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Todo: is this class necessary? Overlap between VisualMarkerTask and ARTask might not be
// too useful
public abstract class ARTaskDriverBase<T> : PlayerTaskDriver<T>
    where T : PlayerTask
{
    public ARTaskDriverBase(ResourceActivationContext context, T task)
		: base(context, task)
	{
        
	}

    public virtual bool TargetsAreVisible { get; protected set; }

    protected abstract bool CheckAnyTargetsVisible();
    protected abstract bool CheckAllTargetsVisible();

    protected virtual void ShowTask()
    {
        UpdateState();
    }

    protected virtual void HideTask()
    {
        if (IsTakeTask)
        {
            //RemoveCollectiblesFromTargets();
        }
        else
        {
            ARViewManager.Instance.RemoveInteractiveCollectibles(ActivationContext.InstanceId);
        }
    }

    public override void Start()
    {
        if (!ActivationContext.IsOpen)
        {
            return;
        }

        ThreadHelper.Instance.CallOnMainThread(ShowTask);

        base.Start();
    }

    public override void Stop()
    {
        ThreadHelper.Instance.CallOnMainThread(HideTask);

        base.Stop();
    }

    protected virtual void CheckAndPlaceItems()
    {
        var wereVisible = TargetsAreVisible;
        TargetsAreVisible = CheckAnyTargetsVisible();

        if (!wereVisible && TargetsAreVisible)
        {
            var collectible = FirstCollectible;

            if (Inventory.Instance.GetCount(collectible.Id) > 0)
            {
                if (collectible != null)
                {
                    ARViewManager.Instance.AddInteractiveCollectible(ActivationContext.InstanceId, collectible, () =>
                    {
                        Action();
                    });
                }
            }
        }
        else if (wereVisible && !TargetsAreVisible)
        {
            ARViewManager.Instance.RemoveInteractiveCollectibles(ActivationContext.InstanceId);
        }
    }

    protected void UpdateState()
    {
        if (IsGiveTask)
        {
            //CheckAndPlaceItems();
        }
        else
        {
            if (Task.Action == "track")
            {
                if (CheckAllTargetsVisible())
                {
                    Action();
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface ILocationMinigameDriver : IMinigameDriver
{
    string ActionButtonText { get; }
    
    string OutOfRangeActionButtonText { get; }

    void ShowTask();

    bool ShowMapAnnotation { get; }

    bool ShowActionButton { get; }

    void HideTask();
}

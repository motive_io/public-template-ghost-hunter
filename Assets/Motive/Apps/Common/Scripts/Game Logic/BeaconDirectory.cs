﻿using Motive.AR.Models;
using Motive.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class BeaconDirectory : SingletonAssetDirectory<BeaconDirectory, Beacon>
{
    public Beacon GetBeaconByIdent(string identifierKey)
    {
        return GetItemsWhere(i => i.IdentifierKey == identifierKey).FirstOrDefault();
    }
}

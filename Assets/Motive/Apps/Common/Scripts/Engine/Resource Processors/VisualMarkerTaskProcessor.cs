﻿using UnityEngine;
using System.Collections;
using Motive.AR.Models;
using Motive.Core.Scripting;

#if VUFORIA_ANDROID_SETTINGS || VUFORIA_IOS_SETTINGS
public class VisualMarkerTaskProcessor : ScriptResourceProcessor<VisualMarkerTask> {
    public override void ActivateResource(Motive.Core.Scripting.ResourceActivationContext context, VisualMarkerTask resource)
    {
        TaskManager.Instance.ActivateVisualMarkerTask(context, resource);
    }

    public override void DeactivateResource(Motive.Core.Scripting.ResourceActivationContext context, VisualMarkerTask resource)
    {
        TaskManager.Instance.DeactivateTask(context.InstanceId);
    }

    public override void UpdateResource(Motive.Core.Scripting.ResourceActivationContext context, VisualMarkerTask resource)
    {
        TaskManager.Instance.UpdateTask(context, resource);
    }
}
#endif
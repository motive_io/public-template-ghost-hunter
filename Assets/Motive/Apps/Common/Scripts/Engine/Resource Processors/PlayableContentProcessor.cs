﻿using UnityEngine;
using System.Collections;
using Motive.Core.Scripting;
using Motive.Gaming.Models;

public class PlayableContentProcessor : ScriptResourceProcessor<PlayableContent> {

    public override void ActivateResource(Motive.Core.Scripting.ResourceActivationContext context, PlayableContent resource)
    {
        PlayableContentHandler.Instance.Play(context, resource);
    }

    public override void DeactivateResource(Motive.Core.Scripting.ResourceActivationContext context, PlayableContent resource)
    {
        PlayableContentHandler.Instance.StopPlaying(context);
    }
}

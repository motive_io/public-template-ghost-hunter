﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Motive.Gaming.Models;
using Motive.Core.Scripting;


public class ObjectiveCompleterProcessor : ScriptResourceProcessor<ObjectiveCompleter>
{
    public override void ActivateResource(ResourceActivationContext context, ObjectiveCompleter resource)
    {
        if (resource.Objective != null && !context.IsClosed)
        {
            TaskManager.Instance.CompleteObjective(resource.Objective);

            context.Close();
        }
    }
}

﻿using Motive.Unity.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Motive.Core.Scripting;
using Motive.Gaming.Models;

namespace Motive.Gaming.Engine
{
	public class WalletCurrencyLimitProcessor : ThreadSafeScriptResourceProcessor<WalletCurrencyLimit>
	{
		public override void ActivateResource(ResourceActivationContext context, WalletCurrencyLimit resource)
		{
			if (context.IsFirstActivation)
			{
				Wallet.Instance.SetLimits(resource);
			}
		}
	}
}

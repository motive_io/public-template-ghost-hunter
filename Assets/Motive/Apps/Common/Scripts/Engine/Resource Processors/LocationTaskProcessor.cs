﻿using UnityEngine;
using System.Collections;
using Motive.AR.Models;
using Motive.Core.Scripting;

public class LocationTaskProcessor : ScriptResourceProcessor<LocationTask> {
    public override void ActivateResource(Motive.Core.Scripting.ResourceActivationContext context, LocationTask resource)
    {
        TaskManager.Instance.ActivateLocationTask(context, resource);
    }

    public override void DeactivateResource(Motive.Core.Scripting.ResourceActivationContext context, LocationTask resource)
    {
        TaskManager.Instance.DeactivateTask(context.InstanceId);
    }

    public override void UpdateResource(Motive.Core.Scripting.ResourceActivationContext context, LocationTask resource)
    {
        TaskManager.Instance.UpdateTask(context, resource);
    }
}

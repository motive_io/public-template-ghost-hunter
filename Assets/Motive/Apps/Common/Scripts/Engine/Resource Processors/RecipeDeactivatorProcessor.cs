﻿using Motive.AR.Models;
using Motive.Core.Scripting;
using System.Collections;
using System.Collections.Generic;
using Motive.Gaming.Models;
using UnityEngine;

public class RecipeDeactivatorProcessor : ScriptResourceProcessor<RecipeDeactivator>
{
	public override void ActivateResource(ResourceActivationContext context, RecipeDeactivator resource)
	{
		if (!context.IsClosed)
		{
			if (resource.RecipeReferences != null)
			{
				foreach (var r in resource.RecipeReferences)
				{
					ActivatedRecipeManager.Instance.DeactivateRecipe(r.ObjectId);
				}
			}

			context.Close();
		}
	}
}
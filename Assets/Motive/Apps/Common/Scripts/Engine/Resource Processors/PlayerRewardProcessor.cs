﻿using UnityEngine;
using System.Collections;
using Motive.Unity.Scripting;
using Motive.Gaming.Models;
using Motive.UI.Framework;

public class PlayerRewardProcessor : ThreadSafeScriptResourceProcessor<PlayerReward> 
{
    public override void ActivateResource(Motive.Core.Scripting.ResourceActivationContext context, PlayerReward resource)
    {
        if (!context.IsClosed)
        {
            RewardManager.Instance.ActivatePlayerReward(resource);
            context.Close();
        }
    }
}

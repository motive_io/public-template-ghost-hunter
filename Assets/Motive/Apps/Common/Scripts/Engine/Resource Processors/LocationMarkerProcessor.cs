﻿using Motive.AR.Models;
using Motive.Unity.Maps;
using Motive.Unity.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class LocationMarkerProcessor : ThreadSafeScriptResourceProcessor<LocationMarker>
{
    public override void ActivateResource(Motive.Core.Scripting.ResourceActivationContext context, LocationMarker resource)
    {
        MapMarkerAnnotationHandler.Instance.AddLocationMarker(context.InstanceId, resource);

        base.ActivateResource(context, resource);
    }

    public override void DeactivateResource(Motive.Core.Scripting.ResourceActivationContext context, LocationMarker resource)
    {
        MapMarkerAnnotationHandler.Instance.RemoveLocationMarker(context.InstanceId);

        base.DeactivateResource(context, resource);
    }

    public override void UpdateResource(Motive.Core.Scripting.ResourceActivationContext context, LocationMarker resource)
    {
        DeactivateResource(context, resource);
        ActivateResource(context, resource);

        base.UpdateResource(context, resource);
    }
}

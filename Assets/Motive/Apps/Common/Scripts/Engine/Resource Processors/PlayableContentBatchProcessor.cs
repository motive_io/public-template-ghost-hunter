﻿using UnityEngine;
using System.Collections;
using Motive.Unity.Scripting;
using Motive.Gaming.Models;

public class PlayableContentBatchProcessor : ThreadSafeScriptResourceProcessor<PlayableContentBatch> {

    public override void ActivateResource(Motive.Core.Scripting.ResourceActivationContext context, PlayableContentBatch resource)
    {
        if (!context.IsClosed)
        {
            PlayableContentHandler.Instance.Play(context, resource);
        }
    }

    public override void DeactivateResource(Motive.Core.Scripting.ResourceActivationContext context, PlayableContentBatch resource)
    {
        PlayableContentHandler.Instance.StopPlaying(context);
    }
}

﻿using Motive.Gaming.Models;
using Motive.Unity.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class PlayerTaskProcessor : ThreadSafeScriptResourceProcessor<PlayerTask>
{
    public override void ActivateResource(Motive.Core.Scripting.ResourceActivationContext context, PlayerTask resource)
    {
        TaskManager.Instance.ActivatePlayerTask(context, resource);
    }

    public override void DeactivateResource(Motive.Core.Scripting.ResourceActivationContext context, PlayerTask resource)
    {
        TaskManager.Instance.DeactivateTask(context.InstanceId);
    }
}

﻿using Motive.AR.Models;
using Motive.Unity.Maps;
using Motive.Unity.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Location3DAssetProcessor : ThreadSafeScriptResourceProcessor<Location3DAsset>
{
    public override void ActivateResource(Motive.Core.Scripting.ResourceActivationContext context, Location3DAsset resource)
    {
        MapMarkerAnnotationHandler.Instance.AddLocation3DAsset(resource);

        base.ActivateResource(context, resource);
    }

    public override void DeactivateResource(Motive.Core.Scripting.ResourceActivationContext context, Location3DAsset resource)
    {
        MapMarkerAnnotationHandler.Instance.RemoveLocation3DAsset(resource);

        base.DeactivateResource(context, resource);
    }
}

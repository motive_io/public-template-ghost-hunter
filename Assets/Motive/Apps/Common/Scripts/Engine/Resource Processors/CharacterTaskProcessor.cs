﻿using UnityEngine;
using System.Collections;
using Motive.Unity.Scripting;
using Motive.Gaming.Models;

public class CharacterTaskProcessor : ThreadSafeScriptResourceProcessor<CharacterTask> {

    public override void ActivateResource(Motive.Core.Scripting.ResourceActivationContext context, CharacterTask resource)
    {
        TaskManager.Instance.ActivateCharacterTask(context, resource);
    }

    public override void DeactivateResource(Motive.Core.Scripting.ResourceActivationContext context, CharacterTask resource)
    {
        TaskManager.Instance.DeactivateTask(context.InstanceId);
    }
}

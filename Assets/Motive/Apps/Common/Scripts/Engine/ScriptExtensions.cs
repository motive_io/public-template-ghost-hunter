﻿using UnityEngine;
using System.Collections;
using Motive.Core.Scripting;
using Motive.AR.Scripting;
using Motive.Core.Json;
using Motive.AR.LocationServices;
using Motive.AR.Beacons;
using Motive.Gaming.Models;
using Motive.AR.Models;
using Motive.UI;
using Motive.UI.Models;
using Motive.Gaming.Engine;
using Motive.Core.Models;
using Motive.Attractions.Models;

public class ScriptExtensions {

    public static void Initialize(ILocationManager locationManager, IBeaconManager beaconManager, ICompass compass)
    {
        // This code bootstraps the Motive script engine. First, give the script engine
        // a path that it can use for storing state.
        var smPath = StorageManager.GetGameStorageManager().GetManager("scriptManager");

        ScriptEngine.Instance.Initialize(smPath);

        // This intializes the Alternate Reality and Location Based features.
        ARComponents.Instance.Initialize(smPath, locationManager, beaconManager, compass, null);

        // This tells the JSON reader how to deserialize various object types based on
        // the "type" field.
        JsonTypeRegistry.Instance.RegisterType("motive.core.scriptDirectoryItem", typeof(ScriptDirectoryItem));
        JsonTypeRegistry.Instance.RegisterType("motive.core.appVersionCondition", typeof(AppVersionCondition));

		JsonTypeRegistry.Instance.RegisterType("motive.ui.notification", typeof(Notification));

        JsonTypeRegistry.Instance.RegisterType("motive.gaming.character", typeof(Character));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.collectible", typeof(Collectible));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.assignment", typeof(Assignment));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.taskObjectiveCompleteCondition", typeof(TaskObjectiveCompleteCondition));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.objectiveActivator", typeof(ObjectiveActivator));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.objectiveCompleter", typeof(ObjectiveCompleter));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.playerTask", typeof(PlayerTask));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.characterTask", typeof(CharacterTask));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.playableContent", typeof(PlayableContent));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.playableContentBatch", typeof(PlayableContentBatch));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.characterMessage", typeof(CharacterMessage));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.screenMessage", typeof(ScreenMessage));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.inventoryCollectibles", typeof(InventoryCollectibles));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.inventoryCollectibleLimit", typeof(InventoryCollectibleLimit));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.playerReward", typeof(PlayerReward));
		JsonTypeRegistry.Instance.RegisterType("motive.gaming.walletCurrency", typeof(WalletCurrency));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.walletCurrencyLimit", typeof(WalletCurrencyLimit));
        JsonTypeRegistry.Instance.RegisterType("motive.gaming.savePoint", typeof(SavePoint));

		JsonTypeRegistry.Instance.RegisterType("motive.gaming.recipe", typeof(Recipe));
		JsonTypeRegistry.Instance.RegisterType("motive.gaming.recipeActivator", typeof(RecipeActivator));
		JsonTypeRegistry.Instance.RegisterType("motive.gaming.recipeDeactivator", typeof(RecipeDeactivator));
		JsonTypeRegistry.Instance.RegisterType("motive.gaming.recipeActivatedCondition", typeof(RecipeActivatedCondition));

        JsonTypeRegistry.Instance.RegisterType("motive.gaming.inventoryCondition", typeof(InventoryCondition));
		JsonTypeRegistry.Instance.RegisterType("motive.gaming.walletCondition", typeof(WalletCondition));

		JsonTypeRegistry.Instance.RegisterType("motive.gaming.canCraftCollectibleCondition", typeof(CanCraftCollectibleCondition));
		JsonTypeRegistry.Instance.RegisterType("motive.gaming.canExecuteRecipeCondition", typeof(CanExecuteRecipeCondition));
		JsonTypeRegistry.Instance.RegisterType("motive.gaming.canCompleteTaskCondition", typeof(CanCompleteTaskCondition));
		JsonTypeRegistry.Instance.RegisterType("motive.gaming.canCompleteObjectiveTaskCondition", typeof(CanCompleteObjectiveTaskCondition));

        JsonTypeRegistry.Instance.RegisterType("motive.ar.beaconTask", typeof(BeaconTask));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.locationTask", typeof(LocationTask));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.visualMarkerTask", typeof(VisualMarkerTask));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.locationMarker", typeof(LocationMarker));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.location3dAsset", typeof(Location3DAsset));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.arCatcherMinigame", typeof(ARCatcherMinigame));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.mapZoomCommand", typeof(MapZoomCommand));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.storyTagLocationTypes", typeof(StoryTagLocationType));

        JsonTypeRegistry.Instance.RegisterType("motive.ar.arLocationCollectionMechanic", typeof(ARLocationCollectionMechanic));
        JsonTypeRegistry.Instance.RegisterType("motive.ar.mapLocationCollectionMechanic", typeof(MapLocationCollectionMechanic));

        JsonTypeRegistry.Instance.RegisterType("motive.ui.interfaceUpdate", typeof(InterfaceUpdate));
        JsonTypeRegistry.Instance.RegisterType("motive.ui.interfaceDirector", typeof(InterfaceDirector));
        JsonTypeRegistry.Instance.RegisterType("motive.ui.objectInspector", typeof(ObjectInspector));

        JsonTypeRegistry.Instance.RegisterType("motive.attractions.locationAttraction", typeof(LocationAttraction));
        JsonTypeRegistry.Instance.RegisterType("motive.attractions.locationAttractionActivator", typeof(LocationAttractionActivator));
        JsonTypeRegistry.Instance.RegisterType("motive.attractions.locationAttractionInteractible", typeof(LocationAttractionInteractible));
        JsonTypeRegistry.Instance.RegisterType("motive.attractions.locationAttractionContent", typeof(LocationAttractionContent));

        // The Script Resource Processors take the resources from the script processor and
        // direct them to the game components that know what to do with them.
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.core.scriptLauncher", new ScriptLauncherProcessor());

        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.ui.objectInspector", new ObjectInspectorProcessor());

        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.assignment", new AssignmentProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.objectiveActivator", new ObjectiveActivatorProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.objectiveCompleter", new ObjectiveCompleterProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.playerTask", new PlayerTaskProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.characterTask", new CharacterTaskProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.inventoryCollectibles", new InventoryCollectiblesProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.inventoryCollectibleLimit", new InventoryCollectibleLimitProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.walletCurrency", new WalletCurrencyProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.walletCurrencyLimit", new WalletCurrencyLimitProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.playableContent", new PlayableContentProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.playableContentBatch", new PlayableContentBatchProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.playerReward", new PlayerRewardProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.savePoint", new SavePointProcessor());
		ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.recipeActivator", new RecipeActivatorProcessor());
		ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.gaming.recipeDeactivator", new RecipeDeactivatorProcessor());

        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.ar.locationTask", new LocationTaskProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.ar.locationMarker", new LocationMarkerProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.ar.location3dAsset", new Location3DAssetProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.ar.locativeAudioContent", new LocativeAudioContentProcessor());
#if VUFORIA_ANDROID_SETTINGS || VUFORIA_IOS_SETTINGS
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.ar.visualMarkerTask", new VisualMarkerTaskProcessor());
#endif

        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.attractions.locationAttractionActivator", new LocationAttractionActivatorProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.attractions.locationAttractionInteractible", new LocationAttractionInteractibleProcessor());
        ScriptEngine.Instance.RegisterScriptResourceProcessor("motive.attractions.locationAttractionContent", new LocationAttractionContentProcessor());

        // Register a condition monitor that knows how to handle inventory conditions.
		ScriptEngine.Instance.RegisterConditionMonitor(new RecipeActivatedConditionMonitor());
		ScriptEngine.Instance.RegisterConditionMonitor(new AppVersionConditionMonitor());
        ScriptEngine.Instance.RegisterConditionMonitor(new TaskObjectiveCompleteConditionMonitor());
        ScriptEngine.Instance.RegisterConditionMonitor(new InventoryConditionMonitor());
		ScriptEngine.Instance.RegisterConditionMonitor(new WalletConditionMonitor());
		ScriptEngine.Instance.RegisterConditionMonitor(new CanCraftCollectibleConditionMonitor());
		ScriptEngine.Instance.RegisterConditionMonitor(new CanExecuteRecipeConditionMonitor());
		ScriptEngine.Instance.RegisterConditionMonitor(new CanCompleteTaskConditionMonitor());
		ScriptEngine.Instance.RegisterConditionMonitor(new CanCompleteObjectiveTaskConditionMonitor());
    }
}

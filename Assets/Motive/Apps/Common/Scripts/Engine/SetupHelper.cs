﻿using Motive;
using Motive.AR.LocationServices;
using Motive.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static partial class SetupHelper 
{
    public static void SetUpFoursquare(string clientId, string secret, string locationStoryTagMap)
    {
        Catalog<FoursquareCategoryMap> categoryMap = null;
        Catalog<StoryTagLocationType> storyTags = null;

        WebServices.Instance.AddCatalogLoad<FoursquareCategoryMap>("motive.ar", "foursquare_category_map", (catalog) =>
        {
            categoryMap = catalog;
        }, false);

        WebServices.Instance.AddCatalogLoad<StoryTagLocationType>(locationStoryTagMap, (catalog) =>
        {
            storyTags = catalog;
        });

        AppManager.Instance.OnLoadComplete((callback) =>
        {
            // Use Foursquare - we can add to this.
            FoursquareService.Instance.Initialize(
                clientId,
                secret,
                categoryMap,
                storyTags,
                (success) =>
                {
                    if (success)
                    {
                        LocationCacheDriver.Instance.AddSearchProvider(FoursquareService.Instance.GetSearchProvider(FoursquareSearchIntent.Browse));

                        LocationCacheDriver.Instance.Start();
                    }

                    callback();
                });
        });
    }

    internal static void SetUpLocationARDefaults()
    {
        Platform.Instance.Initialize();

        WebServices.Instance.Initialize();

        ScriptExtensions.Initialize(
            Platform.Instance.LocationManager,
            Platform.Instance.BeaconManager,
            ForegroundPositionService.Instance.Compass);

        //ARScriptExtensions.Initialize();

        DebugPlayerLocation.Instance.Initialize();
        ForegroundPositionService.Instance.Initialize();
    }
}

﻿using UnityEngine;
using System.Collections;
using Motive.Core.Scripting;
using Motive.Core.Utilities;
using System;
using Motive.Gaming.Models;

namespace Motive.Gaming.Engine
{
	public class WalletConditionMonitor : SynchronousConditionMonitor<WalletCondition>
	{
		public WalletConditionMonitor() : base("motive.gaming.walletCondition")
		{
			Wallet.Instance.Updated += Instance_Updated;
		}

		private void Instance_Updated(object sender, EventArgs e)
		{
			CheckWaitingConditions();
		}

		public override bool CheckState(FrameOperationContext fop, WalletCondition condition, out object[] results)
		{
			results = null;

			if (condition.CurrencyCount == null)
			{
				return false;
			}

			int count = Wallet.Instance.GetCount(condition.CurrencyCount.Currency);

			return MathHelper.CheckNumericalCondition(condition.Operator, count, condition.CurrencyCount.Count);
		}
	}
}
using UnityEngine;
using System.Collections;
using Motive.Core.Utilities;
using Motive.UI.Framework;
using System;
using System.Collections.Generic;
using Motive.Core.WebServices;
using Motive.Core.Scripting;
using Motive;

public class AppManager : Singleton<AppManager>
{
    public event EventHandler Started;
    public event EventHandler ScriptsStarted;
    public event EventHandler Initialized;
    public event EventHandler LaunchFailed;

    private List<Action<Action>> m_onLoadActions = new List<Action<Action>>();

    public bool IsInitialized { get; private set; }

    public void Reset()
    {
        Reset(false);
    }

    public void Reload()
    {
        var loadingPanel = PanelManager.Instance.Push<LoadingPanel>();

        ScriptManager.Instance.StopAllProcessors(() =>
        {
            ReloadFromServer(loadingPanel);
        });
    }

    void ReloadFromServer(LoadingPanel loadingPanel)
    {
        WebServices.Instance.ReloadFromServer(
            () =>
            {
                Action onComplete = () =>
                {
                    if (loadingPanel)
                    {
                        loadingPanel.OnClose = () =>
                        {
                            try
                            {
                                ScriptManager.Instance.RunScripts();

                                if (ScriptsStarted != null)
                                {
                                    ScriptsStarted(this, EventArgs.Empty);
                                }
                            }
                            catch (Exception x)
                            {
                                if (LaunchFailed != null)
                                {
                                    LaunchFailed(this, new UnhandledExceptionEventArgs(x, false));
                                }
                            }
                        };

                        loadingPanel.MediaReady();
                    }

                };

                if (m_onLoadActions.Count > 0)
                {
                    BatchProcessor iter = new BatchProcessor(m_onLoadActions.Count);

                    iter.OnComplete(onComplete);

                    foreach (var action in m_onLoadActions)
                    {
                        action(() => { iter++; });
                    }
                }
                else
                {
                    onComplete();
                }

                Platform.Instance.DownloadsComplete();
            },
            (reason) =>
            {
                loadingPanel.DownloadError(reason);
            });
    }

    public void Start()
    {
        var loadingPanel = PanelManager.Instance.Push<LoadingPanel>();

        if (!IsInitialized)
        {
            WebServices.Instance.DownloadError += (sender, args) =>
            {
                if (LaunchFailed != null)
                {
                    LaunchFailed(this, args);
                }
            };

            IsInitialized = true;

            if (Initialized != null)
            {
                Initialized(this, EventArgs.Empty);
            }
        }

        ReloadFromServer(loadingPanel);

        if (Started != null)
        {
            Started(this, EventArgs.Empty);
        }

        Platform.Instance.FireInteractionEvent("appStart");
    }

    public void OnLoadComplete(Action handler)
    {
        OnLoadComplete((callback) =>
        {
            handler();
            callback();
        });
    }

    public void OnLoadComplete(Action<Action> handler)
    {
        m_onLoadActions.Add(handler);
    }

    public void Reset(bool skipReload)
    {
        var loadingPanel = PanelManager.Instance.Push<LoadingPanel>();

        ScriptManager.Instance.Reset(() =>
        {
            if (skipReload)
            {
                ScriptManager.Instance.RunScripts();
            }
            else
            {
                ReloadFromServer(loadingPanel);
            }
        });
    }

    public void Nuke()
    {
        ScriptManager.Instance.Abort();

        StorageManager.DeleteGameFolder();

        Application.Quit();
    }
}
